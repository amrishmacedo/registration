from __future__ import print_function

from flask import request, jsonify
from flask_cors import cross_origin
from datetime import timedelta
import dateutil.parser

from app import app, requires_auth, handle_error
from config import CLIENT_CALENDAR_ID
from app.api.fireapi import fireapi
from app.api.gcalendar import calapi, get_calendar_id


def get_gevent(client, event_id):
    """
    Get event data from the google calendar
        
    :param client: 
    :param event_id: 
    :return: 
    """
    gevent = None
    calendar_id = ''

    event, err = fireapi.events().get(event_id=event_id)
    if event is not None:
        # get calendar and id
        calendar_id, err = get_calendar_id(client)
        if calendar_id is not None:
            if 'id_in_calendar' in event:
                id_in_calendar = event['id_in_calendar']
                if id_in_calendar != '':
                    gevent, err = calapi.events_get(event_id=id_in_calendar, calendar_id=calendar_id)

    return gevent, event, calendar_id


@app.route("/api/events")
@cross_origin(headers=['Content-Type', 'Authorization'])
def get_all_events():
    """
    get all events which are stored in firebase
    :return: 
    """
    events, err = fireapi.events().list()
    if events is None:
        return handle_error({'code': 'error', 'description': err}, 400)

    return jsonify({'events': events})


@app.route("/api/events/<project_id>")
@cross_origin(headers=['Content-Type', 'Authorization'])
def get_event_list(project_id):
    """
    get event list of certain project
    
    :param project_id: 
    :return: 
    """
    project, err = fireapi.projects().get(project_id)
    if project is None:
        return handle_error({'code': 'error', 'description': err}, 400)

    events, err = fireapi.projects().get_event_list(project['key'])
    if events is None:
        return handle_error({'code': 'error', 'description': err}, 400)

    return jsonify({'events': events})


@app.route("/api/project/<project_id>/events/register", methods=['POST'])
@cross_origin(headers=['Content-Type', 'Authorization'])
@cross_origin(headers=['Access-Control-Allow-Origin', '*'])
@requires_auth
def register_events(project_id):
    """
    
    :param project_id: 
    :return: 
    """
    data = request.json

    if 'email' not in data or 'project_key' not in data:
        return handle_error({'code': 'error', 'description': 'invalid request'}, 400)

    email = data['email']
    project_key = data['project_key']

    # get project
    project, err = fireapi.projects().get_by_key(project_key)
    if project is None:
        return handle_error({'code': 'error', 'description': err}, 400)

    # get calendar and id
    if (CLIENT_CALENDAR_ID is not None) and CLIENT_CALENDAR_ID != '':
        calendar_id = CLIENT_CALENDAR_ID
    else:
        # get calendar which is associated with the client
        calendar, err = calapi.calendars_find(summary=project['client'])
        if calendar is None:
            return handle_error({'code': 'error', 'description': 'client: {} {}'.format(project['client'], err)}, 400)
        calendar_id = calendar['id']

    # get participant by email
    participant, err = fireapi.participants().get_by_email(email)
    if err != '':
        return handle_error({'code': 'error', 'description': err}, 400)
    elif participant is None:
        return handle_error({'code': 'error', 'description': 'no existing participant'}, 400)

    # currently registered events for the user
    old_events = participant['event']

    # newly registered events
    new_events = data['events']

    # update selected flag
    for event in new_events:
        event_id = event['event_id']
        old_events[event_id] = event['selected']

    # add or update event on google calendar

    # get events for the project
    prj_events, err = fireapi.projects().get_events(project_key)
    if prj_events is None:
        return handle_error({'code': 'error', 'description': err}, 400)

    # check the event in the cur_events
    for eid, selected in old_events.iteritems():
        if eid not in prj_events:
            # skip the event not belongs in the project
            continue

        event = prj_events[eid]
        id_in_calendar = ''

        if 'guests' in event:
            guests = event['guests']
        else:
            guests = []

        if 'id_in_calendar' in event:
            id_in_calendar = event['id_in_calendar']
            if id_in_calendar != '':
                gevent, err = calapi.events_get(event_id=id_in_calendar, calendar_id=calendar_id)
                if gevent is None:
                    if err != '404':
                        return handle_error({'code': 'error', 'description': err}, 400)
                    # there are no event on google calendar
                    id_in_calendar = ''
                elif 'status' in gevent:
                    # update calendar
                    if gevent['status'] == 'cancelled':
                        id_in_calendar = ''

        if id_in_calendar == '':
            # not created|cancelled|removed event
            if selected:
                # if user select the event then register the event on Google Calendar
                event_data, err = calapi.new_event(event_data=event, organizer=project['client'], attendees_emails=[email])
                if event_data is None:
                    return handle_error({'code': 'error', 'description': err}, 400)

                gevent, err = calapi.events_insert(event_data=event_data, calendar_id=calendar_id)
                if gevent is None:
                    return handle_error({'code': 'error', 'description': err}, 400)

                id_in_calendar = gevent['id']

            # save gc_id
            if event['id_in_calendar'] != id_in_calendar:
                event['id_in_calendar'] = id_in_calendar
                result, err = fireapi.events().save(event)
                if err != '':
                    return handle_error({'code': 'error', 'description': err}, 400)

        else:
            found = False

            # add or remove attendance to confirmed event
            attendees = gevent['attendees']
            for attendee in attendees:
                if attendee['email'] == email:
                    found = True
                    break

            changed = True
            if found and not selected:
                attendees.remove(attendee)
            elif not found and selected:
                attendees.append({'email': email})
            else:
                # do nothing
                changed = False

            if changed:
                # check attendees for the event
                if len(attendees) == 0:
                    # the event not need any more, so remove it on calendar
                    gevent, err = calapi.events_delete(event_id=id_in_calendar, calendar_id=calendar_id)
                    if err != '':
                        return handle_error({'code': 'exception', 'description': err}, 400)
                    event['id_in_calendar'] = id_in_calendar
                    result, err = fireapi.events().save(event)
                    if err != '':
                        return handle_error({'code': 'error', 'description': err}, 400)

                else:
                    # update google calendar with new event
                    gevent, err = calapi.events_patch(event_id=id_in_calendar, event_data=gevent, calendar_id=calendar_id)
                    if gevent is None:
                        return handle_error({'code': 'exception', 'description': err}, 400)

        # print(eid, selected, guests)

        changed = True
        if email in guests and not selected:
            guests.remove(email)
        elif email not in guests and selected:
            guests.append(email)
        else:
            changed = False

        if changed:
            # update guests
            result, err = fireapi.events().update_guests(event['project_key'], event['event_id'], guests)
            if result is None:
                return handle_error({'code': 'error', 'description': err}, 400)

    # register updated events to participant
    registered_events, err = fireapi.participants().register_events(email, old_events)
    if registered_events is None:
        return handle_error({'code': 'error', 'description': err}, 400)

    return jsonify(registered_events)


@app.route("/api/events/<event_id>/record", methods=['PUT'])
@cross_origin(headers=['Content-Type', 'Authorization'])
@cross_origin(headers=['Access-Control-Allow-Origin', '*'])
@requires_auth
def update_event_record(event_id):
    """
    update the record field of the event identified by event_id

    :param event_id: 
    :return: 
    """
    # session['profile'] = request.json
    payload = request.json
    # check the profile exists in FireBase
    if 'project_key' not in payload:
        return handle_error({'code': 'error', 'description': 'no project_key in the data'}, 400)

    project_key = payload['project_key']
    record = payload['record']
    record = fireapi.events().update_record(project_key, event_id, record)
    return jsonify(record)


@app.route("/api/events/<event_id>", methods=['PUT'])
@cross_origin(headers=['Content-Type', 'Authorization'])
@cross_origin(headers=['Access-Control-Allow-Origin', '*'])
@requires_auth
def update_event(event_id):
    """
    Update event field which is identified by event_id
    
    :param event_id: 
    :return: 
    """
    data = request.json

    # check the profile exists in FireBase
    if 'project_key' not in data:
        return handle_error({'code': 'error', 'description': 'no project_key in the data'}, 400)

    project_key = data['project_key']
    project, err = fireapi.projects().get_by_key(project_key)
    if project is None:
        return handle_error({'code': 'error', 'description': err}, 400)

    # update event
    gevent, event, calendar_id = get_gevent(client=project['client'], event_id=event_id)
    if gevent is None:
        return handle_error({'code': 'exception', 'description': 'not found'}, 400)

    # update calendar event's properties
    changed = False
    if 'name' in data:
        gevent['summary'] = data['name']
        event['name'] = data['name']
        changed = True
    if 'startdatetime' in data:
        if 'duration' in data:
            start_date_time = dateutil.parser.parse(data['startdatetime'])
            end_date_time = start_date_time + timedelta(minutes=data['duration'])
            gevent['start']['dateTime'] = start_date_time.isoformat()
            gevent['end']['dateTime'] = end_date_time.isoformat()
            event['startdatetime'] = data['startdatetime']
            event['duration'] = data['duration']
            changed = True
        else:
            return handle_error({'code': 'exception', 'description': 'duration need'}, 400)

    if changed:
        gevent, err = calapi.events_patch(event_id=gevent['id'], event_data=gevent, calendar_id=calendar_id)
        if gevent is None:
            return handle_error({'code': 'exception', 'description': err}, 400)

        # update firebase event
        events = fireapi.events()
        event, err = events.update(project_key, event_id, event)
        if event is None:
            return handle_error({'code': 'exception', 'description': err}, 400)
    else:
        return handle_error({'code': 'exception', 'description': 'not changed'}, 400)

    return jsonify(event)
