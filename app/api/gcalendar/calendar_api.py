from __future__ import print_function

import httplib2
from oauth2client.client import GoogleCredentials
from googleapiclient import discovery, errors
from datetime import datetime, timedelta
from requests import HTTPError
import logging
import dateutil.parser

from authorize import get_credentials


class CalendarAPI:
    service = None

    def __init__(self):
        pass

    def authenticate_by_api_key(self, developer_key):
        self.service = discovery.build('calendar', 'v3', developerKey=developer_key)

    def authenticate_by_client_secret(self):
        credentials = get_credentials()
        http = credentials.authorize(httplib2.Http())
        self.service = discovery.build('calendar', 'v3', http=http)

    def authenticate_by_application_default(self):
        credentials = GoogleCredentials.get_application_default()
        self.service = discovery.build('calendar', 'v3', credentials=credentials)

    # def authenticate_by_service_account(self):
    #     scope = 'https://www.googleapis.com/auth/calendar'
    #     credentials = AppAssertionCredentials(scope=scope)
    #     http = credentials.authorize(httplib2.Http(memcache))
    #     self.service = discovery.build('calendar', 'v3', http=http)

    @staticmethod
    def new_event(event_data, organizer, attendees_emails=[]):
        try:
            # create new event
            start_date_time = dateutil.parser.parse(event_data['startdatetime'])
            end_date_time = start_date_time + timedelta(minutes=event_data['duration'])

            print(start_date_time.isoformat())
            print(end_date_time.isoformat())

            body = {
                'summary': event_data['name'],
                'start': {
                    'dateTime': start_date_time.isoformat()
                },
                'end': {
                    'dateTime': end_date_time.isoformat()
                },
                'organizer': {
                    'displayName': organizer
                },
                'reminders': {
                    'useDefault': True
                },
                'source': {
                    'title': 'RVIBE',
                    'url': 'https://rviberegistration.appspot.com'
                }
            }

            attendees = []
            for email in attendees_emails:
                attendees.append({'email': email})

            body['attendees'] = attendees

        except Exception as e:
            logging.exception(e.message)
            return None, e.message

        return body, ''

    def calendars_list(self):
        calendars = []
        try:
            page_token = None
            while True:
                calendar_list = self.service.calendarList().list(pageToken=page_token).execute()
                calendars += calendar_list.get('items', [])
                page_token = calendar_list.get('nextPageToken')
                if not page_token:
                    break

            return calendars, ''

        except Exception as err:
            return None, err.message

    def calendars_find(self, summary):
        try:
            page_token = None
            while True:
                calendar_list = self.service.calendarList().list(pageToken=page_token).execute()
                for calendar_list_entry in calendar_list['items']:
                    if calendar_list_entry['summary'] == summary:
                        return calendar_list_entry, ''
                page_token = calendar_list.get('nextPageToken')
                if not page_token:
                    break

            return None, 'Calendar not found'

        except Exception as err:
            if err.message == '':
                return None, err.strerror
            else:
                return None, err.message

    def events_list(self, calendar_id='primary'):
        try:
            now = datetime.utcnow().isoformat() + 'Z'  # 'Z' indicates UTC time
            events_result = self.service.events().list(
                calendarId=calendar_id, timeMin=now, maxResults=10, singleEvents=True,
                orderBy='startTime').execute()
            events = events_result.get('items', [])

            if not events:
                return [], None

            return events, None
        except Exception as err:
            return None, err.message

    def events_get(self, event_id, calendar_id='primary'):
        try:
            event = self.service.events().get(
                calendarId=calendar_id, eventId=event_id).execute()

            return event, None
        except errors.HttpError as err:
            return None, str(err.resp['status'])

        except Exception as err:
            logging.exception(err.message)
            return None, err.message

    def events_patch(self, event_id, event_data, calendar_id='primary'):
        try:
            updated_event = self.service.events().patch(
                calendarId=calendar_id, eventId=event_id, body=event_data).execute()

            return updated_event, None
        except Exception as err:
            logging.exception(err.message)
            return None, err.message

    def events_insert(self, event_data, calendar_id='primary'):
        try:
            event = self.service.events().insert(
                calendarId=calendar_id,
                sendNotifications=True,
                body=event_data).execute()

            return event, None

        except HTTPError as err:
            logging.exception(err.message)
            return None, err.message

        except Exception as err:
            logging.exception(err.message)
            return None, err.message

    def events_update(self, event_data, calendar_id='primary'):
        try:
            event = self.service.events().insert(
                calendarId=calendar_id,
                sendNotifications=True,
                body=event_data).execute()

            return event, None

        except Exception as err:
            logging.exception(err.message)
            return None, err.message

    def events_delete(self, event_id, calendar_id='primary'):
        try:
            event = self.service.events().delete(
                calendarId=calendar_id,
                eventId=event_id,
                sendNotifications=True).execute()

            return event, ''

        except Exception as err:
            logging.exception(err.message)
            return None, err.message
