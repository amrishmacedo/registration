from __future__ import print_function

from app.api.gcalendar.calendar_api import CalendarAPI
from app.config import GOOGLE_API_AUTH_METHOD, GOOGLE_API_KEY, CLIENT_CALENDAR_ID


calapi = CalendarAPI()
if GOOGLE_API_AUTH_METHOD == 'API_KEY':
    calapi.authenticate_by_api_key(GOOGLE_API_KEY)
elif GOOGLE_API_AUTH_METHOD == 'CLIENT_SECRET':
    calapi.authenticate_by_client_secret()
elif GOOGLE_API_AUTH_METHOD == 'APPLICATION_DEFAULT':
    calapi.authenticate_by_application_default()
# elif auth_mode == 'SERVICE_ACCOUNT':
#     calapi.authenticate_by_service_account()


def get_calendar_id(client):
    if (CLIENT_CALENDAR_ID is not None) and CLIENT_CALENDAR_ID != '':
        calendar_id = CLIENT_CALENDAR_ID
    else:
        calendar, err = calapi.calendars_find(summary=client)
        if calendar is None:
            return None, 'not exists'
        calendar_id = calendar['id']

    return calendar_id, ''
