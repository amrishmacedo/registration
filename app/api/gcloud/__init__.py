from __future__ import print_function

import cloudstorage as gcs
import io
import cStringIO

from google.cloud import storage


class GCloudAPI:
    client = None
    bucket = None

    def __init__(self, auth_mode):
        if auth_mode == 'CLIENT_SECRET':
            # Enable Storage
            self.client = storage.Client()
        else:
            credentials = GoogleCredentials.get_application_default()
            self.client = storage.Client(credentials=credentials)

        # Reference an existing bucket.
        self.bucket = self.client.get_bucket('rviberegistration.appspot.com')

    def upload(self, blob_name, file_obj, content_type, file_size):
        blob = self.bucket.blob(blob_name)
        if blob is None:
            return None
        else:
            try:
                blob.upload_from_file(file_obj=file_obj, content_type=content_type, size=file_size)
                url = blob.public_url
                print(url)
                return url
            except Exception as e:
                print(e)
                return None

    def download(self, blob_name):
        # Download a file from your bucket.
        blob = self.bucket.get_blob(blob_name)
        blob.download_as_string()

    def get_blob(self, blob_name):
        return self.bucket.get_blob(blob_name)

# gcloudapi = GCloudAPI(auth_mode)
