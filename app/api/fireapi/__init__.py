from __future__ import print_function

from app.config import FIREBASE_SECRET, FIREBASE_EMAIL, FIREBASE_DSN, FIREBASE_DATA_ROOT

from fire_api import FireAPI

fireapi = FireAPI(FIREBASE_SECRET, FIREBASE_EMAIL, FIREBASE_DSN, FIREBASE_DATA_ROOT)
