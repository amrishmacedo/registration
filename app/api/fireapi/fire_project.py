from __future__ import print_function

import logging
from fire_model import FireModel


class FireProject(FireModel):
    path = '/project'

    def __init__(self, api):
        FireModel.__init__(self, api)

    def list(self):
        projects = []
        try:
            result = self.api.get(self.url(self.path), None)

            for key, prj in result.iteritems():
                prj['key'] = key
                projects.append(prj)

            return projects, None
        except Exception as ex:
            logging.error('{}'.format(ex))
            return None, ex.message

    @staticmethod
    def events(project):
        key = project['key']
        pid = project['projectid']
        events = []
        event_node = project['event']
        for event_id, event in event_node.iteritems():
            event['project_key'] = key
            event['project_id'] = pid
            event['event_id'] = event_id
            events.append(event)

        return events

    def get(self, project_id):
        project = None
        try:
            result = self.api.get(self.url(self.path), None)

            for key, prj in result.iteritems():
                if 'projectid' in prj:
                    if prj['projectid'] == project_id:
                        prj['key'] = key
                        events = FireProject.events(prj)
                        # order by <startdatetime> field
                        events = sorted(events, key=lambda event: event['startdatetime'])
                        prj['events'] = events
                        del prj['event']
                        project = prj
                        break

            return project, None

        except Exception as ex:
            logging.error('{}'.format(ex))
            return None, ex.message

    def get_by_key(self, project_key):
        try:
            location = '{}/{}'.format(self.path, project_key)
            project = self.api.get(self.url(location), None)
            return project, None

        except Exception as ex:
            logging.error('{}'.format(ex))
            return None, ex.message

    def get_events(self, project_key):
        location = '{}/{}/event'.format(self.path, project_key)
        events = {}
        try:
            result = self.api.get(self.url(location), None)

            for eid, event in result.iteritems():
                events[eid] = event

            return events, None
        except Exception as ex:
            logging.error('{}'.format(ex))
            return None, ex.message

    def get_event_list(self, project_key):
        location = '{}/{}/event'.format(self.path, project_key)
        events = []
        try:
            result = self.api.get(self.url(location), None)

            for eid, event in result.iteritems():
                if 'event_id' not in event:
                    event['event_id'] = eid
                elif event['event_id'] != eid:
                    event['event_id'] = eid

                events.append(event)

            events = sorted(events, key=lambda event: event['startdatetime'])
            return events, None
        except Exception as ex:
            logging.error('{}'.format(ex))
            return None, ex.message

    def insert_event(self, project_key, event):
        location = '{}/{}/event'.format(self.path, project_key)
        try:
            result = self.api.put(self.url(location), event['event_id'], event)
            return result, ''
        except Exception as ex:
            logging.error('{}'.format(ex))
            return None, ex.message

    def update_event(self, project_key, event):
        """
        update event of the project with project_key
        @:param project_key
        """
        location = '{}/{}/event'.format(self.path, project_key)
        try:
            result = self.api.post(self.url(location), event)
            return result, ''
        except Exception as ex:
            logging.error('{}'.format(ex))
            return None, ex.message