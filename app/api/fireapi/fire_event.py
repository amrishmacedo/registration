from __future__ import print_function

import logging

from fire_model import FireModel


class FireEvent(FireModel):
    # own properties
    active = 1
    event_id = ''
    created = ''
    duration = 0
    name = ''
    startdatetime = ''
    updatedby = ''

    # parent id
    project_id = ''

    # event id on Google Calendar
    id_in_calendar = ''

    def __init__(self, api):
        """
        
        :param api: 
        """
        FireModel.__init__(self, api)

    @staticmethod
    def datetime_to_event_id(project_id, start_date_time):
        """
        
        :param project_id: 
        :param start_date_time: 
        :return: 
        """
        json_datetime = FireModel.to_json_datetime(start_date_time).replace('"', '')
        event_id = 'PID' + project_id + 'EID' + json_datetime
        return event_id

    def list(self):
        """
        
        :return: 
        """
        events = {}
        try:
            result = self.api.get(self.url('/project'), None)

            for key, prj in result.iteritems():
                event_node = prj['event']
                for eid, event in event_node.iteritems():
                    event['project_key'] = key
                    event['project_id'] = prj['projectid']
                    event['event_id'] = eid
                    events[eid] = event

            return events, None
        except Exception as ex:
            logging.error('{}'.format(ex))
            return None, ex.message

    def get(self, event_id):
        """
        
        :param event_id: 
        :return: 
        """
        try:
            result = self.api.get(self.url('/project'), None)

            for key, prj in result.iteritems():
                event_node = prj['event']
                for eid in event_node:
                    if eid == event_id:
                        return event_node[eid], None
            return None, 'not found'
        except Exception as ex:
            logging.error('{}'.format(ex))
            return None, ex.message

    def save(self, event):
        """
        
        :param event: 
        :return: 
        """
        try:
            project_key = event['project_key']
            location = '/project/{}/event'.format(project_key)
            if event['event_id'] is None:
                print(event)
                return None, 'event id not exists'
            else:
                result = self.api.put(self.url(location), event['event_id'], event)
                return result, ''

        except Exception as ex:
            logging.error('{}'.format(ex))
            return None, ex.message

    def update_record(self, project_key, event_id, record):
        """
        create new event and add to the project
        
        :param project_key: 
        :param event_id: 
        :param record: 
        :return: 
        """
        location = '/project/{}/event/{}'.format(project_key, event_id)
        try:
            result = self.api.put(self.url(location), 'record', record)
            return result, ''

        except Exception as ex:
            logging.error('{}'.format(ex))
            return None, ex.message

    def update_guests(self, project_key, event_id, guests):
        """
        
        :param project_key: 
        :param event_id: 
        :param event: 
        :return: 
        """
        location = '/project/{}/event/{}'.format(project_key, event_id)
        try:
            result = self.api.put(self.url(location), 'guests', guests)
            return result, ''

        except Exception as ex:
            logging.error('{}'.format(ex))
            return None, ex.message

    def update(self, project_key, event_id, event):
        """
        
        :param project_key: 
        :param event_id: 
        :param event: 
        :return: 
        """
        location = '/project/{}/event'.format(project_key)
        try:
            result = self.api.put(self.url(location), event_id, event)
            return result, ''

        except Exception as ex:
            logging.error('{}'.format(ex))
            return None, ex.message