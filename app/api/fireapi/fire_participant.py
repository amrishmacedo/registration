from __future__ import print_function

from requests import HTTPError
import logging

from fire_model import FireModel


class FireParticipant(FireModel):
    path = '/participant'

    def __init__(self, api):
        FireModel.__init__(self, api)

    def list(self):
        try:
            participants = self.api.get(self.url(self.path), None)
            return participants, ''
        except HTTPError as ex:
            if ex.response.status_code != 400:
                return None, ex.message
        except Exception as ex:
            logging.error('exception: {}'.format(ex))
            return None, ex.message

    def get_by_email(self, email):
        participant_id = self.email_with_underscore(email)
        return self.get(participant_id)

    def get(self, participant_id):
        # get participants
        participant = None
        try:
            participant = self.api.get(self.url(self.path), participant_id)
            if participant is None:
                # yet not registered account
                return None, ''
            if 'event' not in participant:
                participant['event'] = {}
        except HTTPError as ex:
            if ex.response.status_code != 400:
                return None, ex.message
        except Exception as ex:
            logging.error('exception: {}'.format(ex))
            return None, ex.message

        return participant, ''

    def add(self, data):
        # add new participant
        participant_id = self.email_with_underscore(data['email'])
        try:
            participant = self.api.put(self.url(self.path), participant_id, data)
            # first get user's event data
            if 'event' not in participant:
                participant['event'] = {}
            logging.debug('ADD PARTICIPANT: {}'.format(participant))
        except Exception as ex:
            logging.error('exception: {}'.format(ex))
            return None, ex.message

        return participant, ''

    def remove(self, email):
        # remove the participant
        participant_id = self.email_with_underscore(email)
        try:
            participant = self.api.delete(self.url(self.path), participant_id)
        except Exception as ex:
            logging.exception('{}'.format(ex))
            return None, ex.message

        return participant, ''

    def register_events(self, email, events):
        """
        register events for the user having email
        while patch the event for the user, we need to update event also.
        """
        participant_id = self.email_with_underscore(email)

        try:
            location = '{path}/{participant_id}'.format(path=self.path, participant_id=participant_id)
            result = self.api.put(self.url(location), 'event', events)
        except Exception as ex:
            logging.exception('{}'.format(ex))
            return None, ex.message

        return events, ''
