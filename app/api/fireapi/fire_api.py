from __future__ import print_function

from firebase.firebase import FirebaseApplication, FirebaseAuthentication
import json

from fire_model import FireModel
from fire_project import FireProject
from fire_event import FireEvent
from fire_participant import FireParticipant


class FireAPI:
    api = None

    m_project = None
    m_event = None
    m_participant = None
    m_record = None

    def __init__(self, secret, email, dsn, data_root):
        auth = FirebaseAuthentication(secret=secret, email=email, debug=True, admin=False)
        self.api = FirebaseApplication(dsn, auth)

        FireModel.data_root = data_root

        self.m_project = FireProject(self.api)
        self.m_event = FireEvent(self.api)
        self.m_participant = FireParticipant(self.api)

        print('FireAPI.__init__')

    @staticmethod
    def from_json_datetime(json_datetime):
        return json.loads(json_datetime)

    @staticmethod
    def from_json_object(json_obj):
        json.loads(json_obj)
        return

    def projects(self):
        return self.m_project

    def participants(self):
        return self.m_participant

    def events(self):
        return self.m_event