from __future__ import print_function

import logging
import datetime
import json


class FireModel:
    api = None
    path = ''
    data_root = ''

    def __init__(self, api):
        self.api = api

    @staticmethod
    def email_with_underscore(email):
        return email.replace(".", "_")

    @staticmethod
    def to_json_datetime(datetime_string):
        # defaults to current datetime if nothing is passed
        # if date and time is passed it gets both back
        # if only date is passed it only gets json date back
        # python format looks like 2020-08-09T11:24:20
        # http://pubs.opengroup.org/onlinepubs/009695399/functions/strptime.html
        try:
            date_handler = lambda obj: (
                obj.isoformat()
                if isinstance(obj, datetime.datetime)
                   or isinstance(obj, datetime.date)
                else None
            )
            if datetime_string is None:
                return json.dumps(datetime.datetime.now(), default=date_handler)

            if datetime_string.find(':') > 0:
                datetime_string = datetime.datetime.strptime(datetime_string, '%m/%d/%Y %H:%M')
            else:
                datetime_string = datetime.datetime.strptime(datetime_string, '%m/%d/%Y').date()

            return json.dumps(datetime_string, default=date_handler)
        except Exception as e:
            print(e)

        return json.dumps(datetime.datetime.now(), default=date_handler)

    def url(self, location):
        if FireModel.data_root == '':
            return location
        else:
            return self.data_root + '/' + location

    def search(self, location, key):
        try:
            result = self.api.get(self.url(location), key)
            result['key'] = key
            return result, ''
        except Exception as ex:
            logging.error('{}'.format(ex))
            return None, ex.message

    def insert(self, location, obj):
        try:
            result = self.api.post(self.url(location), obj)
            return result, ''
        except Exception as ex:
            logging.error('{}'.format(ex))
            return None, ex.message

    def update(self, location, key, obj):
        try:
            result = self.api.put(self.url(location), key, obj)
            return result, ''
        except Exception as ex:
            logging.error('{}'.format(ex))
            return None, ex.message

    def delete(self, location, key):
        try:
            result = self.api.delete(self.url(location), key)
            return result.json(), ''
        except Exception as ex:
            logging.error('{}'.format(ex))
            return None, ex.message
