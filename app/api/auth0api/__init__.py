from __future__ import print_function

import urllib2
import json
from app.utils.requests import RequestWithMethod

from app.config import AUTH0_DOMAIN, AUTH0_CLIENT_ID, AUTH0_CLIENT_SECRET
from app.config import NON_INTERACTIVE_CLIENT_ID, NON_INTERACTIVE_CLIENT_SECRET
from app.config import REDIRECT_URI


def _add_headers(request):
    request.add_header('Content-Type', 'application/json')


def get_token_info(code):
    token_payload = {
        'client_id': AUTH0_CLIENT_ID,
        'client_secret': AUTH0_CLIENT_SECRET,
        'redirect_uri': REDIRECT_URI,
        'code': code,
        'grant_type': 'authorization_code'
    }

    token_url = "{domain}/oauth/token".format(domain=AUTH0_DOMAIN)

    request = urllib2.Request(token_url, json.dumps(token_payload))
    _add_headers(request)

    try:
        response = urllib2.urlopen(request)

        return json.loads(response.read())
    except urllib2.HTTPError as e:
        print("Error in get_token_info")
        print(e)
        print(json.loads(e.read()))


def get_profile(token_info):
    user_url = "{domain}/userinfo?access_token={access_token}" \
        .format(domain=AUTH0_DOMAIN, access_token=token_info['access_token'])

    request = urllib2.Request(user_url)
    _add_headers(request)

    try:
        response = urllib2.urlopen(request)

        return json.loads(response.read())
    except urllib2.HTTPError as e:
        print("Error in get_profile")
        print(e)
        print(json.loads(e.read()))


def get_management_token():
    url = "{domain}/oauth/token".format(domain=AUTH0_DOMAIN)
    request_data = {
        "grant_type": "client_credentials",
        "client_id": NON_INTERACTIVE_CLIENT_ID,
        "client_secret": NON_INTERACTIVE_CLIENT_SECRET,
        "audience": "{domain}/api/v2/".format(domain=AUTH0_DOMAIN)
    }

    request = urllib2.Request(url, json.dumps(request_data))
    request.add_header('Content-Type', 'application/json')

    try:
        response = urllib2.urlopen(request)

        return json.loads(response.read())
    except urllib2.HTTPError as e:
        print("Error in get_management_token")
        print(e)
        print(json.loads(e.read()))


def get_users(management_token):
    url = "{domain}/api/v2/users".format(domain=AUTH0_DOMAIN)
    authorization = "{type} {token}" \
        .format(type=management_token['token_type'], token=management_token['access_token'])

    request = urllib2.Request(url)
    request.add_header('Content-Type', 'application/json')
    request.add_header('Authorization', authorization)

    try:
        response = urllib2.urlopen(request)

        return json.loads(response.read())
    except urllib2.HTTPError as e:
        print("Error in get_user")
        print(e)
        print(json.loads(e.read()))


def get_user(management_token, user_id):
    url = "{domain}/api/v2/users/{id}".format(domain=AUTH0_DOMAIN, id=user_id)
    authorization = "{type} {token}" \
        .format(type=management_token['token_type'], token=management_token['access_token'])

    request = urllib2.Request(url)
    request.add_header('Content-Type', 'application/json')
    request.add_header('Authorization', authorization)

    try:
        response = urllib2.urlopen(request)

        return json.loads(response.read())
    except urllib2.HTTPError as e:
        print("Error in get_user")
        print(e)
        print(json.loads(e.read()))


def _prepare_management_request(url, method='GET', payload=None):
    management_token = get_management_token()

    request_url = "{domain}/api/v2/{url}".format(domain=AUTH0_DOMAIN, url=url)
    authorization = "{type} {token}" \
        .format(type=management_token['token_type'], token=management_token['access_token'])

    if payload is None:
        request = RequestWithMethod(method, request_url)
    else:
        request = RequestWithMethod(method, request_url, json.dumps(payload))

    request.add_header('Content-Type', 'application/json')
    request.add_header('Authorization', authorization)

    return request


def get_user_metadata(user_id):
    management_token = get_management_token()
    user = get_user(management_token, user_id)

    return user["user_metadata"]


def update_user_metadata(user_id, metadata):
    url = "users/{id}".format(id=user_id)
    user_metadata = {
        "user_metadata": metadata
    }

    request = _prepare_management_request(url, 'PATCH', user_metadata)

    try:
        urllib2.urlopen(request)

        return {
            "success": True,
            "data": {}
        }
    except urllib2.HTTPError as e:
        print("Error in update_user_metadata")
        print(e)
        message = json.loads(e.read())
        print(message)

        return {
            "success": False,
            "error_message": message
        }
