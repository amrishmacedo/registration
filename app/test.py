from __future__ import print_function

from flask import request, jsonify, session
from flask_cors import cross_origin
import logging

from app import app, requires_auth


@app.route("/api/private")
@cross_origin(headers=['Content-Type', 'Authorization'])
@cross_origin(headers=['Access-Control-Allow-Origin', '*'])
@requires_auth
def get_private_test():
    return jsonify({'message': "Hello from a private endpoint! You DO need to be authenticated to see this."})


@app.route("/api/session", methods=['GET', 'POST'])
@cross_origin(headers=['Content-Type', 'Authorization'])
def test_session():
    if request.method == 'GET':
        if 'param' in session:
            if session['param'] is not None:
                return jsonify(session['param'])
            else:
                del session['param']

        return jsonify({'message': 'No param in session'})
    else:
        logging.debug(request.json)
        session['param'] = request.json
        return jsonify({'message': 'set param'})