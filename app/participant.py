from __future__ import print_function

from flask import request, jsonify
from flask_cors import cross_origin

from app import app, requires_auth, handle_error
from app.api.fireapi import fireapi


@app.route("/api/participants", methods=['GET'])
@cross_origin(headers=['Content-Type', 'Authorization'])
@cross_origin(headers=['Access-Control-Allow-Origin', '*'])
@requires_auth
def get_participants():
    try:
        participants, err = fireapi.participants().list()
        if err != '':
            return handle_error({'code': 'error', 'description': err}, 400)

        result = []
        for key in participants:
            participant = participants[key]
            participant['id'] = key
            result.append(participant)

    except Exception as err:
        return handle_error({'code': 'exception', 'description': err.message}, 400)

    return jsonify(result)


@app.route("/api/participants/<participant_id>")
@cross_origin(headers=['Content-Type', 'Authorization'])
@cross_origin(headers=['Access-Control-Allow-Origin', '*'])
@requires_auth
def get_participant(participant_id):
    try:
        participant, err = fireapi.participants().get(participant_id)
        if err != '':
            return handle_error({'code': 'error', 'description': err}, 400)

    except Exception as err:
        return handle_error({'code': 'exception', 'description': err.message}, 400)

    return jsonify(participant)

