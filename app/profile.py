from __future__ import print_function

from flask import request, jsonify
from flask_cors import cross_origin
import time

from app import app, requires_auth, handle_error
from app.api.fireapi import fireapi


@app.route("/api/profile", methods=['POST'])
@cross_origin(headers=['Content-Type', 'Authorization'])
@cross_origin(headers=['Access-Control-Allow-Origin', '*'])
@requires_auth
def set_profile():
    """
    register auth0 profile

    :return: 
    """
    # session['profile'] = request.json
    profile = request.json
    # check the profile exists in FireBase
    try:
        email = profile['email']
        participant, err = fireapi.participants().get_by_email(email)
        if err != '':
            return handle_error({'code': 'error', 'description': err}, 400)

        if participant is None:
            # if there are no participant having the email, add new participant
            name = ''
            if 'nickname' in profile:
                name = profile['name']
            elif 'user_metadata' in profile:
                user_metadata = profile['user_metadata']
                if 'full_name' in user_metadata:
                    name = user_metadata['full_name']

            if name == '':
                name = profile['name']

            data = {
                'active': 1,
                'name': name,
                'email': email,
                'event': {},
                'created': time.strftime("%Y-%m-%d"),
                'updatedby': 'RVIBE',
                'auth0_id': profile['user_id']
            }
            participant, err = fireapi.participants().add(data)
            if participant is None:
                return handle_error({'code': 'error', 'description': err}, 400)

    except Exception as err:
        return handle_error({'code': 'exception', 'description': err.message}, 400)

    # check & refresh auth0_id
    if 'auth0_id' not in participant:
        participant['auth0_id'] = profile['user_id']
    elif participant['auth0_id'] != profile['user_id']:
        return handle_error({'code': 'error', 'description': 'invalid auth0 id'}, 400)

    return jsonify(participant)


@app.route("/api/profile", methods=['DELETE'])
@cross_origin(headers=['Content-Type', 'Authorization'])
def remove_profile():
    # session.pop('profile')
    profile = request.json
    # check the profile exists in FireBase
    try:
        email = profile['email']
        participant, err = fireapi.participants().get(email)
        if err != '':
            return handle_error({'code': 'error', 'description': err}, 400)

        if participant is None:
            # if there are no participant having the email, add new participant
            participant, err = fireapi.participants().remove(email)
            if participant is None:
                return handle_error({'code': 'error', 'description': err}, 400)

    except Exception as err:
        return handle_error({'code': 'exception', 'description': err.message}, 400)

    return jsonify({'message': 'profile removed'})
