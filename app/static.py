from __future__ import print_function

from flask import send_from_directory, render_template
import re
import logging
from app import app


@app.route('/<regex(".*\.(js|css|woff2|svg|ttf|eot|woff|png|ico)"):path>/')
def send_script(path):
    """
    
    :param path: 
    :return: 
    """
    pattern = r'.*\/(.*\.(js|css|woff2|svg|ttf|eot|woff|png|ico))'
    result = re.match(pattern, path)
    if result is None:
        file_path = path
    else:
        file_path = result.group(1)

    print(path, file_path)
    return send_from_directory('../static', file_path)


@app.route('/<path:path>')
def another_files(path):
    """
    
    :param path: 
    :return: 
    """
    return render_template('index.html')


@app.route('/')
def index_view():
    return render_template('index.html')


@app.errorhandler(500)
def server_error(err):
    # Log the error and stacktrace.
    logging.exception('An error occurred during a request. {}'.format(err))
    return 'An internal error occurred.', 500
