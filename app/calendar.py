from __future__ import print_function

from flask import jsonify
from flask_cors import cross_origin

from app import app, requires_auth, handle_error
from app.api.gcalendar import calapi


@app.route("/api/calendar/list")
@cross_origin(headers=['Content-Type', 'Authorization'])
@cross_origin(headers=['Access-Control-Allow-Origin', '*'])
@requires_auth
def calendar_list():

    # get calendars and id
    calendars, err = calapi.calendars_list()
    if calendars is None:
        return handle_error({'code': 'error', 'description': 'client: {}'.format(err)}, 400)

    return jsonify(calendars)


@app.route("/api/calendar/<calendar_id>")
@cross_origin(headers=['Content-Type', 'Authorization'])
@cross_origin(headers=['Access-Control-Allow-Origin', '*'])
def calendar_info(calendar_id):
    events, err = calapi.events_list(calendar_id)
    if err is not None:
        return handle_error({'code': 'error', 'description': err}, 400)
    else:
        return jsonify({'event': events})


@app.route("/api/calendar/events")
@cross_origin(headers=['Content-Type', 'Authorization'])
def calendar_events_list():
    events, err = calapi.events_list()
    if events is None:
        return handle_error({'code': 'error', 'description': err}, 400)
    else:
        return jsonify({'event': events})


@app.route("/api/calendar/events/<event_id>")
@cross_origin(headers=['Content-Type', 'Authorization'])
def calendar_events_get(event_id):
    event, err = calapi.events_get(event_id)
    if event is None:
        return handle_error({'code': 'error', 'description': err}, 400)
    else:
        return jsonify({'event': event})
