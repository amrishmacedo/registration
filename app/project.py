from __future__ import print_function

from flask import request, jsonify
from flask_cors import cross_origin

from app import app, handle_error, requires_auth
from app.api.fireapi import fireapi


@app.route("/api/projects")
@cross_origin(headers=['Content-Type', 'Authorization'])
def get_projects():
    projects, err = fireapi.projects().list()

    if projects is None:
        return handle_error({'code': 'error', 'description': err}, 400)

    return jsonify({'projects': projects})


@app.route("/api/project/<project_id>")
@cross_origin(headers=['Content-Type', 'Authorization'])
def get_project(project_id):
    project, err = fireapi.projects().get(project_id)
    if project is None:
        return handle_error({'code': 'error', 'description': err}, 400)

    return jsonify({'project': project})


@app.route("/api/project/<project_id>/events", methods=['GET'])
@cross_origin(headers=['Content-Type', 'Authorization'])
@cross_origin(headers=['Access-Control-Allow-Origin', '*'])
@requires_auth
def get_project_events(project_id):
    try:
        """
        if 'profile' not in session:
            # there are no saved profile yet
            return handle_error({'code': 'error', 'description': 'no profile'}, 400)

        profile = session['profile']
        """
        email = request.args.get('email', '')
        if email == '':
            return handle_error({'code': 'error', 'description': 'invalid email'}, 400)

        participant, err = fireapi.participants().get_by_email(email)
        if err != '':
            return handle_error({'code': 'error', 'description': err}, 400)
        elif participant is None:
            return handle_error({'code': 'error', 'description': 'not existing participant'}, 400)

        # first get user's event data
        user_events = participant['event']

        project, err = fireapi.projects().get(project_id)
        if project is None:
            return handle_error({'code': 'error', 'description': err}, 400)

        if len(user_events) == 0:
            return jsonify({'project': project})

        for event in project['events']:
            event_id = event['event_id']
            if event_id in user_events:
                event['registered'] = user_events[event_id]
            else:
                event['registered'] = False

    except Exception as err:
        return handle_error({'code': 'exception', 'description': err.message}, 400)

    return jsonify({'project': project})


@app.route("/api/project/<project_key>/events", methods=['PUT'])
@cross_origin(headers=['Content-Type', 'Authorization'])
@cross_origin(headers=['Access-Control-Allow-Origin', '*'])
@requires_auth
def update_project(project_key):
    """
    
    :param project_key: 
    :return: 
    """
    data = request.json

    projects = fireapi.projects()
    for field, value in data.iteritems():
        if field in ['name']:
            projects.update('project/{}'.format(project_key), field, value)

    project = fireapi.projects().get_by_key(project_key)
    return jsonify({'project': project})
