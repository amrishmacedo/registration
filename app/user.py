from __future__ import print_function

from flask import jsonify
from flask_cors import cross_origin

from app import app, handle_error, requires_auth
from app.api import auth0api


@app.route("/api/users")
@cross_origin(headers=['Content-Type', 'Authorization'])
@cross_origin(headers=['Access-Control-Allow-Origin', '*'])
@requires_auth
def get_users():
    try:
        management_token = auth0api.get_management_token()
        users = auth0api.get_users(management_token)
    except Exception as err:
        return handle_error({'code': 'exception', 'description': err.message}, 400)

    return jsonify(users)


@app.route("/api/users/<user_id>")
@cross_origin(headers=['Content-Type', 'Authorization'])
@cross_origin(headers=['Access-Control-Allow-Origin', '*'])
@requires_auth
def get_user(user_id):
    try:
        management_token = auth0api.get_management_token()
        user = auth0api.get_user(management_token, user_id)

    except Exception as err:
        return handle_error({'code': 'exception', 'description': err.message}, 400)

    return jsonify(user)

