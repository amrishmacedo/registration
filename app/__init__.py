from __future__ import print_function

import jwt

from functools import wraps
from flask import Flask, request, jsonify, _request_ctx_stack

from api.fireapi import FireAPI
from api.gcalendar import CalendarAPI
import logging

from config import FLASK_SECRET_KEY
from config import AUTH0_CLIENT_ID, AUTH0_CLIENT_SECRET

from regex_converter import RegexConverter

app = Flask(__name__, template_folder='../static')
app.url_map.converters['regex'] = RegexConverter
app.secret_key = FLASK_SECRET_KEY

app.config.from_object(__name__)


# Format error response and append status code.
def handle_error(error, status_code):
    resp = jsonify(error)
    resp.status_code = status_code
    return resp


def requires_auth(f):
    """

    :type f: object
    """

    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.headers.get('Authorization', None)
        if not auth:
            return handle_error(
                {'code': 'authorization_header_missing', 'description': 'Authorization header is expected'}, 401)

        parts = auth.split()

        if parts[0].lower() != 'bearer':
            return handle_error(
                {'code': 'invalid_header', 'description': 'Authorization header must start with Bearer'}, 401)
        elif len(parts) == 1:
            return handle_error({'code': 'invalid_header', 'description': 'Token not found'}, 401)
        elif len(parts) > 2:
            return handle_error(
                {'code': 'invalid_header', 'description': 'Authorization header must be Bearer + \s + token'}, 401)

        token = parts[1]
        try:
            payload = jwt.decode(
                jwt=token,
                # key=base64.b64decode(client_secret.replace("_", "/").replace("-", "+")),
                key=AUTH0_CLIENT_SECRET,
                audience=AUTH0_CLIENT_ID
            )
        except jwt.ExpiredSignature:
            return handle_error({'code': 'token_expired', 'description': 'token is expired'}, 401)
        except jwt.InvalidAudienceError:
            return handle_error(
                {'code': 'invalid_audience', 'description': 'incorrect audience, expected: ' + AUTH0_CLIENT_ID}, 401)
        except jwt.DecodeError:
            return handle_error({'code': 'token_invalid_signature', 'description': 'token signature is invalid'}, 401)
        except Exception as ex:
            logging.exception(ex.message)
            return handle_error({'code': 'exception', 'description': 'Unable to parse authentication token.'}, 400)

        _request_ctx_stack.top.current_user = user = payload

        print(user)

        return f(*args, **kwargs)

    return decorated


import project
import event
import profile
import calendar
import static
import test
import user
import participant
