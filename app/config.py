from __future__ import print_function

import os
from os.path import join, dirname
from dotenv import Dotenv
import logging


APPLICATION_ID = 'rviberegistration'
FIREBASE_STORAGE_URL = 'https://firebasestorage.googleapis.com/v0'

AUTH0_CLIENT_ID = ''
AUTH0_CLIENT_SECRET = ''
NON_INTERACTIVE_CLIENT_ID = ''
NON_INTERACTIVE_CLIENT_SECRET = ''
FIREBASE_DSN = ''
FIREBASE_SECRET = ''
FIREBASE_EMAIL = ''
FLASK_SECRET_KEY = ''
GOOGLE_API_AUTH_METHOD = ''
GOOGLE_API_KEY = ''
CLIENT_CALENDAR_ID = ''
FIREBASE_DATA_ROOT = ''

env = None
if 'APPLICATION_ID' in os.environ:
    if not os.environ['APPLICATION_ID'].startswith('dev'):
        env = os.environ

if env is None:
    # localhost
    dotenv_path = join(dirname(__file__), '.env')
    env = Dotenv(dotenv_path)

try:

    AUTH0_DOMAIN = env['AUTH0_DOMAIN']
    AUTH0_CLIENT_ID = env["AUTH0_CLIENT_ID"]
    AUTH0_CLIENT_SECRET = env["AUTH0_CLIENT_SECRET"]
    NON_INTERACTIVE_CLIENT_ID = env["NON_INTERACTIVE_CLIENT_ID"]
    NON_INTERACTIVE_CLIENT_SECRET = env["NON_INTERACTIVE_CLIENT_SECRET"]
    REDIRECT_URI = env["REDIRECT_URI"]

    FIREBASE_DSN = env["FIREBASE_DSN"]
    FIREBASE_SECRET = env["FIREBASE_SECRET"]
    FIREBASE_EMAIL = env["FIREBASE_EMAIL"]
    FIREBASE_DATA_ROOT = env["DATA_ROOT"]

    FLASK_SECRET_KEY = env["FLASK_SECRET_KEY"]

    GOOGLE_API_AUTH_METHOD = env["AUTH_MODE"]
    GOOGLE_API_KEY = env["API_KEY"]

    CLIENT_CALENDAR_ID = env["CLIENT_CALENDAR_ID"]

except IOError as e:
    logging.exception(e.message)
