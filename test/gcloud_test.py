from server.app.gcloud import GCloudAPI

gcloud_api = GCloudAPI('')

blob = gcloud_api.get_blob('414.jpg')
print(blob.path)
print(blob.metadata['firebaseStorageDownloadTokens'])
download_link = 'https://firebasestorage.googleapis.com/v0' + blob.path + \
    '?alt=media&token=' + blob.metadata['firebaseStorageDownloadTokens']
print(download_link)
