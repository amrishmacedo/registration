from __future__ import print_function

import sys
import os

from os.path import join, dirname
from app.api.fireapi import *
from dotenv import Dotenv


env_file_path = join(dirname(__file__), '.env')
env = None

try:
    env = Dotenv(env_file_path)
except IOError as e:
    logging.exception(e.message)
    env = os.environ

try:
    firebase_dsn = env["FIREBASE_DSN"]
    firebase_secret = env["FIREBASE_SECRET"]
    firebase_email = env["FIREBASE_EMAIL"]
except IOError as e:
    logging.error(e.message)

firebase_storage_url = 'https://firebasestorage.googleapis.com/v0'
fireapi = FireAPI(firebase_secret, firebase_email, firebase_dsn)


def create_project(project_file):
    # read project data from file
    with open(project_file) as json_data:
        project = json.load(json_data)
        print(json.dumps(project, indent=2))

    if 'projectid' not in project:
        print('There are no project id')
        return

    project_id = project['projectid']
    prj, err = fireapi.projects().get(project_id)
    if prj is not None:
        print('There are already exists the project with id: ', project_id)
        return

    project['created'] = time.strftime("%Y-%m-%d +0000", time.gmtime())
    events = project['events']

    del project['events']

    result, err = fireapi.projects().insert('project', project)
    if result is None:
        print('\nThe new project couldn\'t be inserted. error: ', err)
        return

    project_key = result['name']
    for event in events:
        event['event_id'] = FireEvent.datetime_to_event_id(project_id, event['startdatetime'])
        event['created'] = time.strftime("%Y-%m-%d")
        event['project_key'] = project_key
        # just for debug
        print(event['event_id'])
        result, err = fireapi.projects().insert_event(project_key, event)
        if result is None:
            print('\nThe event couldn\'t be inserted. error: ', err)
            return

    new_project, err = fireapi.projects().get(project_id)
    if new_project is None:
        print('\nCouldn\'t get tne inserted project. error: ', err)

    print('\nThe new project inserted into database.')
    print(json.dumps(new_project, indent=2))


def update_event(project_key, event):
    """
    this is function for testing purpose only
    """
    fireapi.projects().update_event(project_key, event)


if __name__ == "__main__":
    print('Number of arguments:', len(sys.argv), 'arguments.')
    print('Argument List:', str(sys.argv))

    if len(sys.argv) == 1:
        print('Please specify the input file name')
        exit(1)

    print('The project data will be loaded from:', sys.argv[1])

    try:
        create_project(sys.argv[1])
    except Exception as e:
        print('Exception while read project file:', e)

