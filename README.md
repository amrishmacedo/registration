RVIBE Registration
==================

This is a repository for all the code of RVIBE Registration product (both frontend and backend side).

The project is built on Python Flask and React frameworks.
You'll mostly use this API either for a SPA or a Mobile app. 
If you just want to create a Regular Python WebApp, please check [this other seed project](https://github.com/auth0/auth0-python/tree/master/examples/flask-webapp)

This project is deployed at Google App Engine at https://rviberegistration.appspot.com

## Pre-requirement

* Python
* Google App Engine Python SDK
* npm
* bower

## Installation

```
sudo -i
npm install -g npm@latest
npm install -g rimraf
```

# 3. Build

In order to run the example you need to have `python` and `pip` installed.

You also need to set the client secret and ID of your Auth0 app as environment variables with the following names respectively: `AUTH0_CLIENT_SECRET` and `AUTH0_CLIENT_ID`.

For that, if you just create a file named `.env` in the directory and set the values like the following, the app will just work:

```bash
# .env file
AUTH0_CLIENT_SECRET=myCoolSecret
AUTH0_CLIENT_ID=myCoolClientId
```

Once you've set those 2 enviroment variables:

1. Install the needed dependencies with `pip install -r requirements.txt`
2. Start the server with `python server.py`
3. Try calling [http://localhost:3001/ping](http://localhost:3001/ping)

You can then try to do a GET to [http://localhost:3001/secured/ping](http://localhost:3001/secured/ping) which will throw an error if you don't send the JWT in the header.

    
    git clone
    cd client
    
    npm install
    npm install -g hjs-webpack
    npm install -g webpack
    npm install -g cross-env
    touch .env
    nano .env
    npm run build

    cd ../server
    mkdir lib
    pip install -t lib -r requirements.txt
    
    edit the "app.prod.yaml"

# 4. Deploying the example on Google App Engine Python Standard Environment

(http://stackoverflow.com/questions/38840261/app-engine-deploy-credentials-file-not-writable-opening-in-read-only-mode)

    gcloud config set app/num_file_upload_processes 1
    gcloud app deploy app.prod.yaml --project=rviberegistration --version=test-1
    
# To get the application default credential

gcloud beta auth application-default login
 
 sudo easy_install distribute (fix dotenv issue)
 
 appcfg.py -V 1 --no_cookies  --noauth_local_webserver update app.yaml app.test.yaml
 
# Migrating Traffic

# API Documentation

- Project

```
URL: /api/projects
AUTH: false
METHOD: GET
RESPONSE:
```
```
URL: /api/project/<project_id>
AUTH: false
METHOD: GET
RESPONSE:
```
```
URL: /api/private/project/<project_id>
AUTH: false
METHOD: GET
RESPONSE:
```
```
URL: /api/private/project/<project_key>
AUTH: false
METHOD: GET
RESPONSE:
```
