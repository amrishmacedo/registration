import React, {PropTypes as T} from 'react';
import {browserHistory, router} from 'react-router';
import {connect} from 'react-redux';
import RaisedButton from 'material-ui/RaisedButton';
import Snackbar from 'material-ui/Snackbar';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';
import LinearProgress from 'material-ui/LinearProgress';
import Paper from 'material-ui/Paper';
import {Grid, Row, Col} from 'react-bootstrap';
import ActionHelp from 'material-ui/svg-icons/action/help-outline';
import AuthService from 'utils/AuthService';
import EventTable from './EventTable';
import ProfileMenu from 'components/Profile/ProfileMenu';
import styles from './styles.module.css';

const style = {
  project: {
    margin: 12,
    padding: 12
  },
  button: {
    margin: 12
  }
};

export class Home extends React.Component {
  static propTypes = {
    auth: T.instanceOf(AuthService)
  };

  constructor(props, context) {
    super(props, context);

    this.state = {
      page: 'first',
      dialogMessage: '',
      openMessageDialog: false,
      openSnackBar: false,
      completed: 0,
      projectImageUrl: '',
      progressMode: 'indeterminate',
      messageOfSnackBar: 'Event added to your calendar.',
      selectedEvents: []
    };

    this.project = null;
    this.api = props.api;
  }

  componentWillMount() {
    const {auth} = this.props;

    auth.on('profile_updated', () => {
      this.refreshProject('second');
    });

    auth.on('profile_removed', () => {
      window.location.reload();
    });

    this.refreshProject(this.state.page);
  }

  logout = () => {
    if (this.props.auth.loggedIn()) {
      this.props.auth.logout();
    }
  };
  handleSelection = key => {
    const selectedEvents = [];
    if (key === 'all') {
      for (let i = 0; i < this.project.events.length; i++) {
        selectedEvents.push(i);
      }
    } else if (key === 'none') {
      this.setState({selectedEvents: []});
    } else if (key.length === 0) {
      this.setState({selectedEvents: []});
    } else {
      for (let i = 0; i < key.length; i++) {
        selectedEvents.push(key[i]);
      }
    }
    this.setState({selectedEvents: selectedEvents});
  };
  syncProjectWithEvents = (projectId, page) => {
    const {auth, api} = this.props;
    const profile = auth.getProfile();
    const registeredEvents = [];
    let selectedEvents = localStorage.getItem('selectedEvents');
    const self = this;

    if (typeof selectedEvents !== 'undefined') {
      selectedEvents = [];
    }

    api.projects.getForUser(projectId, profile, res => {
      const project = res.project;

      if (selectedEvents.length === 0) {
        for (let i = 0; i < project.events.length; i++) {
          if (project.events[i].registered) {
            registeredEvents.push(i);
          } else if (selectedEvents.indexOf(i) !== -1) {
            registeredEvents.push(i);
          }
        }
      }
      api.projects.getImageUrl(project.image, (err, url) => {
        if (err) {
          console.log(err);
        }
        project.imageUrl = url;
        self.project = project;
        self.setState({
          completed: 100,
          progressMode: 'determinate',
          page: page,
          selectedEvents: registeredEvents
        });
      });
    }, () => {
      this.project = null;
      browserHistory.push(`/landing`);
    });
  };
  syncProject = (projectId, page) => {
    const {api} = this.props;
    const self = this;
    api.projects.get(projectId, res => {
      const project = res.project;
      api.projects.getImageUrl(project.image, (err, url) => {
        if (err) {
          console.log(err);
        }
        project.imageUrl = url;
        self.project = project;
        self.setState({completed: 100, progressMode: 'determinate', page: page});
      });
    }, err => {
      console.log(err);
    });
  };
  refreshProject = page => {
    const {auth, params} = this.props;
    const projectId = params['project_id'];

    if (typeof projectId === 'undefined') {
      this.setState({page: page});
      return;
    }

    if (auth.loggedIn()) {
      this.syncProjectWithEvents(projectId, page);
    } else {
      this.syncProject(projectId, page);
    }
  };
  registerInFirstPage = () => {
    const {auth, location} = this.props;
    const {selectedEvents} = this.state;

    if (selectedEvents.length === 0) {
      this.showMessageDialog('Please select a least one or more events.');
    } else if (auth.loggedIn()) {
      this.setState({page: 'second'});
    } else {
      localStorage.setItem('selectedEvents', JSON.stringify(selectedEvents));
      localStorage.setItem('redirect_after_login', JSON.stringify(location.pathname));
      auth.login();
    }
  };
  registerInSecondPage = () => {
    const {api} = this.props;
    const {selectedEvents} = this.state;
    const profile = this.props.auth.getProfile();
    const registeredEvents = [];
    const project = this.project;

    project.events.map((event, i) => {
      registeredEvents.push({
        event_id: event.event_id,
        selected: (selectedEvents.indexOf(i) !== -1)
      });
      return true;
    });

    const data = {
      name: profile.name,
      email: profile.email,
      project_key: project.key,
      events: registeredEvents
    };
    const self = this;

    // ToDO: disable register button before register events
    api.events.register(project.projectid, data, () => {
      self.setState({
        messageOfSnackBar: 'Event added to your calendar',
        openSnackBar: true
      });
    }, err => {
      self.setState({
        messageOfSnackBar: `Failed to add the events due to the server problem:\n${err.description}`,
        openSnackBar: true
      });
    });
  };
  showMessageDialog = message => {
    this.setState({
      openMessageDialog: true,
      dialogMessage: message
    });
  };
  closeMessageDialog = () => {
    this.setState({openMessageDialog: false});
  };
  closeSnackBar = () => {
    this.setState({openSnackBar: false});
  };
  cancel = () => {
    this.setState({page: 'first', selectedEvents: []});
  };
  changeSelection = () => {
    this.setState({page: 'first'});
  };
  progress = completed => {
    if (completed > 100) {
      this.setState({completed: 100});
    } else {
      this.setState({completed});
      const diff = Math.random() * 10;
      this.timer = setTimeout(() => this.progress(completed + diff), 1000);
    }
  };
  handleTouchTap = e => {
    e.preventDefault();

    this.setState({
      open: true,
      anchorEl: e.currentTarget
    });
  };
  render() {
    const {auth} = this.props;
    const {page, dialogMessage, openMessageDialog, openSnackBar, progressMode, completed, messageOfSnackBar, selectedEvents} = this.state;
    const messageDialogActions = [
      <FlatButton label="Close" primary={true} onTouchTap={this.closeMessageDialog}/>
    ];
    const project = this.project;
    const profile = auth.getProfile();

    if (project === null) {
      return null;
    }

    const namesup = project.name.split('®');

    return (
      <div>
        <AppBar
          style={{backgroundColor: '#991230', maxHeight: 56}}
          title="RVIBE"
          iconElementRight={
            auth.loggedIn() ?
              <ProfileMenu name={profile.name} picture={profile.picture} logout={this.logout}/> :
              <IconButton><ActionHelp/></IconButton>
          }
          zDepth={2}
        />
        <LinearProgress color={'#b71c1c'} mode={progressMode} value={completed}/>
        <div className={styles.pageContent}>
          <div>
            <Grid className={styles.projectCard}>
              <Row>
                <Col>
                  <div style={{float: 'left'}}>
                    <img src={project.imageUrl}/>
                  </div>
                  <div style={{float: 'right'}}>
                    <Paper style={style.project} zDepth={2} rounded={true} >
                      {
                        namesup.length === 1 ?
                          <h1>{namesup[0]}</h1> :
                          <h1>{namesup[0]}<sup>®</sup>{namesup[1]}</h1>
                      }
                      {project.desc}
                    </Paper>
                  </div>
                </Col>
              </Row>
            </Grid>
            <EventTable
              profile={profile}
              project={project}
              page={page}
              selectedEvents={selectedEvents}
              loggedIn={auth.loggedIn()}
              handleSelection={this.handleSelection}
              showMessageDialog={this.showMessageDialog}
            />
          </div>
          <div className={styles.buttonBar}>
            {
              page === 'first' &&
                <div>
                  <RaisedButton label="Register" primary={true} style={style.button} onTouchTap={this.registerInFirstPage}/>
                </div>
            }
            {
              page === 'second' &&
                <div>
                  <RaisedButton label="Register" primary={true} style={style.button} onTouchTap={this.registerInSecondPage}/>
                  <RaisedButton label="Cancel" primary={true} style={style.button} onTouchTap={this.cancel}/>
                  <RaisedButton label="Change selection" primary={true} style={style.button}
                                onTouchTap={this.changeSelection}/>
                </div>
            }
          </div>
          <Dialog title="Event" actions={messageDialogActions} modal={true} open={openMessageDialog}
                  contentStyle={{width: 400}}>
            {dialogMessage}
          </Dialog>
        </div>
        <Snackbar
          open={openSnackBar}
          message={messageOfSnackBar}
          autoHideDuration={4000}
          onRequestClose={this.closeSnackBar}
        />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    api: state.counter.api
  };
};

export default connect(mapStateToProps, null)(Home);
