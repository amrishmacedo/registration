import React from 'react'
import moment from 'moment-timezone';
import FlatButton from 'material-ui/FlatButton';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn, TableFooter} from 'material-ui/Table';
import Dialog from 'material-ui/Dialog';
import {convertToLocalTime} from 'utils/timezoneCity';
import styles from './styles.module.css';

export class EventTable extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      openRecordDialog: false,
      record: null
    };
    this.api = props.api;
  }
  handleSelection = key => {
    this.props.handleSelection(key);
  };
  isValidRecord(record) {
    if (record && record.url) {
      if (record.url.startsWith('https://firebasestorage.googleapis.com')) {
        return true;
      }
    }
    return false;
  }
  gotoEvent = () => {
    const {profile, project, showMessageDialog} = this.props;
    let fullName = '';
    let url = '';
    const adobeConnect = (project.adobe_connect === undefined) ?
      'https://rvibe.adobeconnect.com/dev_test' :
      project.adobe_connect;

    if (profile === undefined) {
      showMessageDialog('You need to login first.');
    } else {
      if (profile.user_metadata && profile.user_metadata.full_name) {
        fullName = profile.user_metadata.full_name;
      } else if (profile.username) {
        fullName = profile.username;
      } else if (profile.nickname) {
        fullName = profile.nickname;
      }

      if (fullName !== null || fullName !== '') {
        url = `${adobeConnect}?guestName=${fullName}`;
        window.open(url);
      } else {
        showMessageDialog('You have not full name in your profile.');
      }
    }
  };
  viewRecord = event => {
    const {showMessageDialog} = this.props;
    if (event.record) {
      this.setState({
        openRecordDialog: true,
        record: event.record
      });
    } else {
      showMessageDialog('This event has not any record.');
    }
  };
  closeRecordDialog = () => {
    this.setState({openRecordDialog: false});
  };
  render() {
    const {project, page, selectedEvents, loggedIn} = this.props;
    const {openRecordDialog, record} = this.state;
    const videoDialogActions = [
      <FlatButton label="Close" primary={true} onTouchTap={this.closeRecordDialog} />
    ];
    const selectable = (page === 'first');
    const selectedFlag = [];
    const registeredFlag = [];
    const startTime = [];
    const endTime = [];
    const colSpan = loggedIn ? 4 : 3;
    const videoSrc = this.isValidRecord(record) ? record.url : '';

    project.events.map((event, i) => {
      const localTime = convertToLocalTime(event.startdatetime, event.timezone);

      startTime.push(localTime.format('hh:mm A'));
      endTime.push(localTime.add(event.duration, 'm').format('hh:mm A'));

      selectedFlag.push(selectedEvents.indexOf(i) !== -1);
      if (event.registered) {
        registeredFlag.push(1);
      } else if (this.isValidRecord(event.record)) {
        registeredFlag.push(2);
      } else {
        registeredFlag.push(0);
      }
      return true;
    });

    return (
      <div>
         <Table multiSelectable={true} onRowSelection={this.handleSelection}>
            <TableHeader enableSelectAll={selectable} displaySelectAll={selectable} className={styles.eventTableHeader}>
            <TableRow>
              <TableHeaderColumn style={{color: 'black'}}>Event</TableHeaderColumn>
              <TableHeaderColumn style={{color: 'black'}}>Date</TableHeaderColumn>
              <TableHeaderColumn style={{color: 'black'}}>Start</TableHeaderColumn>
              <TableHeaderColumn style={{color: 'black'}}>End</TableHeaderColumn>
              {loggedIn ? <TableHeaderColumn style={{color: 'black'}}>Record</TableHeaderColumn> : null}
            </TableRow>
          </TableHeader>
          <TableBody deselectOnClickaway={false}>
            {project.events.map((event, i) =>
              <TableRow key={event.event_id} selected={selectedFlag[i]} selectable={selectable}>
                <TableRowColumn> {event.name} </TableRowColumn>
                <TableRowColumn>{moment(event.startdatetime).format('dddd, MMMM D, YYYY')}</TableRowColumn>
                <TableRowColumn>{startTime[i]}</TableRowColumn>
                <TableRowColumn>{endTime[i]}</TableRowColumn>
                {
                  loggedIn ?
                    <TableRowColumn>
                      { (registeredFlag[i] === 0) && null }
                      { (registeredFlag[i] === 1) &&
                      <FlatButton label="Go to Event" primary={true} onTouchTap={() => this.gotoEvent()}/>
                      }
                      { (registeredFlag[i] === 2) &&
                      <FlatButton label="Record" primary={true} onTouchTap={() => this.viewRecord(event)}/>
                      }
                    </TableRowColumn> : null
                }
              </TableRow>
            )}
          </TableBody>
          <TableFooter adjustForCheckbox={selectable}>
            <TableRow>
              <TableRowColumn colSpan={colSpan} style={{textAlign: 'center'}}>
              </TableRowColumn>
            </TableRow>
          </TableFooter>
        </Table>
        <Dialog title="Record" actions={videoDialogActions} modal={true} open={openRecordDialog} >
          <div style={{textAlign: 'center'}}>
            <video width="640px" height="320px" controls>
              <source id="videoSource" src={videoSrc} type="video/mp4"/>
            </video>
          </div>
        </Dialog>
      </div>
    );
  }
}

export default EventTable;
