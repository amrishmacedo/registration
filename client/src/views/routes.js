import React from 'react';
import {Route, IndexRedirect} from 'react-router';
import AuthService from 'utils/AuthService';
import Container from './Container';
import Home from './Home/Home';
import Landing from './Landing/Landing';
import Admin from './Admin/Admin';
import Login from './Login/Login';

const auth = new AuthService(__AUTH0_CLIENT_ID__, __AUTH0_DOMAIN__);

const redirectAfterLogin = replace => {
  const url = localStorage.getItem('redirect_after_login');
  if (url) {
    localStorage.removeItem('redirect_after_login');
    replace({pathname: url});
  }
};

const requireAuth = (nextState, replace) => {
  if (auth.loggedIn()) {
    redirectAfterLogin(replace);
  } else {
    localStorage.setItem('redirect_after_login', nextState.location.pathname);
    replace({pathname: '/login'});
  }
};

const requireAdminAuth = (nextState, replace) => {
  if (!auth.isAdmin()) {
    replace({pathname: '/'});
  }
};

export const makeMainRoutes = () => {
  return (
    <Route path="/" component={Container} auth={auth}>
      <IndexRedirect to="landing"/>
      <Route path="landing/:project_id" component={Home}/>
      <Route path="landing" component={Landing} onEnter={requireAuth}/>
      <Route path="admin(/:page)(/:id)" component={Admin} onEnter={requireAdminAuth}/>
      <Route path="login" component={Login}/>
    </Route>
  );
};

export default makeMainRoutes;
