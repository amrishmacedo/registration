import React, {PropTypes as T} from 'react';
import AuthService from 'utils/AuthService';

export class Footer extends React.Component {
  static propTypes = {
    auth: T.instanceOf(AuthService)
  };

  render() {
    const {auth} = this.props;
    const fbUser = auth.getFbUser();

    return (
      <div>
        {__PROJECT_TITLE__}
      </div>
    );
  }
}

export default Footer;
