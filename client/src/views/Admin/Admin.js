import React, {PropTypes as T} from 'react';
import {browserHistory, router} from 'react-router';
import {connect} from 'react-redux';
import {setFirebaseUser} from 'core/Actions';

import Drawer from 'material-ui/Drawer';
import {LeftNav, MenuItem} from 'material-ui';
import {AppBar} from 'material-ui';
import Snackbar from 'material-ui/Snackbar';

import Dashboard from './Dashboard';
import UserPage from './User/UserPage';
import ProjectPage from './Project/ProjectPage';
import EventPage from './Event/EventPage';
import CalendarPage from './Calendar/CalendarPage';
import ParticipantPage from './Participant/ParticipantPage';
import ProfileMenu from 'components/Profile/ProfileMenu';

import AuthService from 'utils/AuthService';

const firebase = require('firebase');

const config = {
  apiKey: __FIREBASE_API_KEY__,
  authDomain: __FIREBASE_AUTH_DOMAIN__,
  databaseURL: __FIREBASE_DATABASE_URL__,
  storageBucket: __FIREBASE_STORAGE_BUCKET__,
  messagingSenderId: __FIREBASE_MESSAGING_SENDER_ID__
};
firebase.initializeApp(config);

const adminMenus = [
  {
    id: 'project',
    label: 'Project',
    route: 'project',
    icon: 'project'
  },
  {
    id: 'calendar',
    label: 'Calendar',
    route: 'calendar',
    icon: 'calendar'
  },
  {
    id: 'user',
    label: 'Users',
    route: 'user',
    icon: 'user'
  },
  {
    id: 'participant',
    label: 'Participants',
    route: 'participant',
    icon: 'participant'
  }
];

export class Admin extends React.Component {
  static propTypes = {
    location: T.object,
    auth: T.instanceOf(AuthService)
  };

  constructor(props, context) {
    super(props, context);
    this.state = {
      openSnackbar: false,
      openDrawer: false
    };
  }

  componentDidMount() {
    const {auth} = this.props;
    const self = this;

    const profile = auth.getProfile();
    if (auth.isAdmin(profile)) {
      firebase.auth().onAuthStateChanged(user => {
        if (user) {
          auth.setFbUser(user);
        } else {
          // No user is signed in.
          auth.clearFbUser();
          self.authenticateWithGoogle();
        }
      });
    } else {
      browserHistory.push('/');
    }
  }

  componentDidUpdate() {
    const {auth} = this.props;
    const fbUser = auth.getFbUser();
    if (fbUser === null || fbUser === undefined) {
      this.authenticateWithGoogle();
    }
  }

  authenticateWithGoogle() {
    const provider = new firebase.auth.GoogleAuthProvider();
    firebase.auth().signInWithRedirect(provider).then(result => {
      // This gives you a Google Access Token. You can use it to access the Google API.
      // var token = result.credential.accessToken;
      // The signed-in user info.
      // var user = result.user;
    }).catch(error => {
      // // Handle Errors here.
      // let errorCode = error.code;
      // let errorMessage = error.message;
      // // The email of the user's account used.
      // let email = error.email;
      // // The firebase.auth.AuthCredential type that was used.
      // let credential = error.credential;
      console.log('error', error);
    });
  }

  logout = () => {
    if (this.props.auth.loggedIn()) {
      this.props.auth.logout();
    }
  };

  handleDrawerToggle = () => this.setState({openDrawer: !this.state.openDrawer});

  handleRequestClose = () => {
    this.setState({openSnackbar: false});
  };

  render() {
    const {auth, api} = this.props;
    const {page, id} = this.props.params;
    const profile = auth.getProfile();
    let messageOfSnackBar = '';
    const fbUser = auth.getFbUser();
    if (fbUser === null || fbUser === undefined) {
      messageOfSnackBar = 'You are not logged in Firebase';
    } else {
      messageOfSnackBar = 'You are logged in Firebase';
    }

    return (
      <div id="page_container">
        <AppBar
          style={{maxHeight: 60}}
          title={__PROJECT_TITLE__ + ' Admin'}
          onLeftIconButtonTouchTap={this.handleDrawerToggle}
          iconElementRight={
            auth.loggedIn() ?
              <ProfileMenu
                name={profile.name}
                picture={profile.picture}
                logout={this.logout}
              /> : null
          }
        />
        <Drawer open={this.state.openDrawer}>
          <AppBar title="Admin"
                  onLeftIconButtonTouchTap={this.handleDrawerToggle}
                  iconClassNameRight="muidocs-icon-navigation-expand-more"/>
          {
            adminMenus.map((menu, i) =>
              <MenuItem
                key={menu.id}
                onTouchTap={() => {
                  browserHistory.push(`/admin/${menu.route}`);
                  this.setState({openDrawer: false});
                }}
              >
                {menu.label}
              </MenuItem>
            )
          }
        </Drawer>
        {
          page === undefined &&
          <Dashboard auth={auth} api={api}/>
        }
        {
          page === 'project' &&
          <ProjectPage auth={auth} id={id}/>
        }
        {
          page === 'event' &&
          <EventPage auth={auth} id={id}/>
        }
        {
          page === 'user' &&
          <UserPage api={api} id={id}/>
        }
        {
          page === 'participant' &&
          <ParticipantPage api={api} id={id}/>
        }
        {
          page === 'calendar' &&
          <CalendarPage auth={auth}/>
        }
        <Snackbar
          open={this.state.openSnackbar}
          message={messageOfSnackBar}
          action="Login with Firebase"
          autoHideDuration={3000}
          onRequestClose={this.handleRequestClose}
        />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    api: state.counter.api
  };
};

export default connect(mapStateToProps, null)(Admin);
