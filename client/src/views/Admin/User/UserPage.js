import React, {PropTypes as T} from 'react';
import {browserHistory, router} from 'react-router';
import ApiService from 'utils/ApiService';
import {connect} from 'react-redux';
import UserList from './UserList';
import UserDetail from './UserDetail';

export class UserPage extends React.Component {
  static propTypes = {
    location: T.object,
    api: T.instanceOf(ApiService)
  };
  render() {
    const {id, api} = this.props;

    return (
      <div>
        {
          (typeof id === 'undefined') ?
            <UserList api={api} location={location}/> : <UserDetail api={api} id={id}/>
        }
      </div>
    );
  }
}

export default UserPage;
