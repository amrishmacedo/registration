import React, {PropTypes as T} from 'react';
import {browserHistory} from 'react-router';
import ApiService from 'utils/ApiService';
import {connect} from 'react-redux';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn, TableFooter} from 'material-ui/Table';
import FlatButton from 'material-ui/FlatButton';

export class UserList extends React.Component {
  static propTypes = {
    location: T.object,
    api: T.instanceOf(ApiService)
  };
  constructor(props, context) {
    super(props, context);
    this.state = {
      users: []
    };
  }
  componentWillMount() {
    this.getUsers();
  }
  getUsers() {
    const {api} = this.props;
    api.users.getAll(users => {
      this.setState({users: users});
    }, err => {
      this.setState({users: []});
    });
  }
  render() {
    const {location} = this.props;
    const {users} = this.state;
    users.map(user => {
      user.roles = 'user';
      if (user.app_metadata) {
        if (user.app_metadata.roles === 'admin') {
          user.roles = 'admin';
        }
      }
      return user;
    });
    return (
      <div>
        <Table multiSelectable={false} onRowSelection={this.handleSelection}>
          <TableHeader enableSelectAll={false} displaySelectAll={false}>
            <TableRow>
              <TableHeaderColumn>User ID</TableHeaderColumn>
              <TableHeaderColumn>Email</TableHeaderColumn>
              <TableHeaderColumn>Name</TableHeaderColumn>
              <TableHeaderColumn>Nick Name</TableHeaderColumn>
              <TableHeaderColumn>Roles</TableHeaderColumn>
              <TableHeaderColumn></TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody deselectOnClickaway={false}>
            {users.map((user, i) =>
              <TableRow key={user.user_id} selectable={true}>
                <TableRowColumn>{user.user_id} </TableRowColumn>
                <TableRowColumn>{user.email}</TableRowColumn>
                <TableRowColumn>{user.name}</TableRowColumn>
                <TableRowColumn>{user.nickname}</TableRowColumn>
                <TableRowColumn>{user.roles}</TableRowColumn>
                <TableHeaderColumn>
                  <FlatButton
                    label="Detail"
                    primary={true}
                    onTouchTap={() => browserHistory.push(location + '/' + user.user_id)}
                  />
                </TableHeaderColumn>
              </TableRow>
            )}
          </TableBody>
        </Table>
      </div>
    );
  }
}

export default UserList;

