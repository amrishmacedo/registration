import React, {PropTypes as T} from 'react';
import {browserHistory, router} from 'react-router';
import ApiService from 'utils/ApiService';
import {connect} from 'react-redux';
import FlatButton from 'material-ui/FlatButton';
import {Grid, Row, Col} from 'react-bootstrap';

export class UserDetail extends React.Component {
  static propTypes = {
    location: T.object,
    api: T.instanceOf(ApiService)
  };
  constructor(props, context) {
    super(props, context);
    this.state = {
      user: {}
    };
  }
  componentWillMount() {
    this.getUser();
  }
  getUser() {
    const {api, id} = this.props;
    api.users.get(id, user => {
      this.setState({user: user});
    }, err => {
      this.setState({user: {}});
    });
  }
  render() {
    const {user} = this.state;

    return (
      <div style={{textAlign: 'center'}}>
        <Grid>
          <Row>
            <Col md={3} />
            <Col md={3}><b>Email :</b></Col>
            <Col md={3}>{user.email}</Col>
            <Col md={3} />
          </Row>
        </Grid>
      </div>
    );
  }
}

export default UserDetail;

