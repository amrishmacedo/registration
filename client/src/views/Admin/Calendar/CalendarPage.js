import React, {PropTypes as T} from 'react';
import {browserHistory, router} from 'react-router';
import AuthService from 'utils/AuthService';
import {connect} from 'react-redux';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn, TableFooter} from 'material-ui/Table';

export class CalendarPage extends React.Component {
  static propTypes = {
    location: T.object,
    auth: T.instanceOf(AuthService)
  };
  constructor(props, context) {
    super(props, context);
    this.state = {
      open: false,
      calendars: []
    };
  }
  componentWillMount() {
    const {api} = this.props;
    api.calendars.getList(calendars => {
      this.setState({calendars: calendars});
    }, err => {
      console.log(err);
    });
  }
  render() {
    const {calendars} = this.state;

    return (
      <div>
        <Table>
          <TableHeader enableSelectAll={false} displaySelectAll={false}>
            <TableRow>
              <TableHeaderColumn>Name</TableHeaderColumn>
              <TableHeaderColumn>Calendar Id</TableHeaderColumn>
              <TableHeaderColumn>Time Zone</TableHeaderColumn>
              <TableHeaderColumn>Role</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody>
            {calendars.map((calendar, i) =>
              <TableRow key={i} selectable={true}>
                <TableRowColumn>{calendar.summary}</TableRowColumn>
                <TableRowColumn>{calendar.id}</TableRowColumn>
                <TableRowColumn>{calendar.timeZone}</TableRowColumn>
                <TableRowColumn>{calendar.accessRole}</TableRowColumn>
              </TableRow>
            )}
          </TableBody>
        </Table>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    api: state.counter.api
  };
};

export default connect(mapStateToProps, undefined)(CalendarPage);
