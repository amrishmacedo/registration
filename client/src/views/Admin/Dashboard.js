import React, {PropTypes as T} from 'react';
import {browserHistory} from 'react-router';
import {connect} from 'react-redux';
import ProjectGridView from 'components/ProjectGridView/ProjectGridView';

export class Dashboard extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      open: false,
      projects: []
    };
  }
  componentDidMount() {
    const {api} = this.props;

    api.projects.getAll(data => {
      this.setState({projects: data.projects});
    }, err => {
      console.log(err);
    });
  }
  onTileClick(id) {
    browserHistory.push(`/admin/project/${id}`);
  }
  render() {
    const {api} = this.props;
    const {projects} = this.state;

    return (
      <div>
        <ProjectGridView api={api} projects={projects} onTileClick={this.onTileClick.bind(this)}/>
      </div>
    );
  }
}

export default Dashboard;
