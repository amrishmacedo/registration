import React from 'react';
import Dialog from 'material-ui/Dialog';
import {Field, reduxForm} from 'redux-form';
import TextField from 'material-ui/TextField';
import Toggle from 'material-ui/Toggle';
import FlatButton from 'material-ui/FlatButton';
import {convertToLocalTime, getTimeOffset} from 'utils/timezoneCity';

const alertDialogStyle = {
  width: '30%',
  maxWidth: 'none'
};

const styles = {
  block: {
    maxWidth: 250
  },
  toggle: {
    marginBottom: 16
  },
  thumbOff: {
    backgroundColor: '#ffcccc'
  },
  trackOff: {
    backgroundColor: '#ff9d9d'
  },
  thumbSwitched: {
    backgroundColor: 'red'
  },
  trackSwitched: {
    backgroundColor: '#ff9d9d'
  },
  labelStyle: {
    color: 'red'
  }
};

export class ProjectDialog extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      projectName: null,
      openAlertDialog: false
    };
    this.alertText = '';
  }
  handleNameChange = (e, name) => {
    this.setState({
      projectName: name
    });
  };
  handleProjectChange = () => {
    const {project} = this.props;
    const {projectName} = this.state;

    if (projectName) {
      const data = {
        projectKey: project.key,
        name: projectName
      };
      this.props.api.projects.update(project.projectkey, data, () => {
        this.alertText = 'The name of the project updated successfully.';
        this.setState({openAlertDialog: true});
      }, err => {
        this.alertText = err.message;
        this.setState({openAlertDialog: true});
      });
    } else {
      this.alertText = 'Please edit the name before submit';
      this.setState({openAlertDialog: true});
    }
  };
  handleCloseAlert = () => {
    this.setState({openAlertDialog: false});
  };
  onCloseDialog = () => {
    this.state = {
      projectName: null,
      openAlertDialog: false
    };
    this.props.handleClose();
  };
  render() {
    const {project, open} = this.props;
    const {openAlertDialog} = this.state;
    const projectName = this.state.projectName || project.name;

    const actions = [
      <FlatButton
        label="Close"
        primary={true}
        onTouchTap={this.onCloseDialog}
      />,
      <FlatButton
        label="Submit"
        primary={true}
        keyboardFocused={true}
        onTouchTap={this.handleProjectChange}
      />
    ];
    const alertActions = [
      <FlatButton
        label="Close"
        primary={true}
        keyboardFocused={true}
        onTouchTap={this.handleCloseAlert}
      />
    ];

    return (
      <Dialog
        title="Project"
        open={open}
        modal={true}
        autoScrollBodyContent={true}
        actions={actions}
      >
        <div>
          <TextField
            id="id-project-name"
            floatingLabelText="Name"
            defaultValue={projectName}
            onChange={this.handleNameChange}
          />
          <Toggle label="Active" style={styles.toggle} toggled={project.active}/>
          <TextField
            id="id-project-client"
            floatingLabelText="Client"
            defaultValue={project.client}
          />
          <TextField
            id="id-project-created"
            floatingLabelText="Created"
            defaultValue={project.created}
          />
          <div id="id-project-desc"><b>Description: </b><p>{project.desc}</p></div>
          <div id="id-project-image"><b>Image: </b><p>{project.image}</p></div>
          <div id="id-project-id"><b>Id: </b><p>{project.projectid}</p></div>
          <div id="id-project-updated-by"><b>Updated By: </b><p>{project.updatedby}</p></div>
        </div>
        <Dialog open={openAlertDialog} actions={alertActions} contentStyle={alertDialogStyle}>
          <div>{this.alertText}</div>
        </Dialog>
      </Dialog>
    );
  }
}

export default ProjectDialog;
