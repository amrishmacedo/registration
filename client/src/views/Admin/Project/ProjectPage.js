import React, {PropTypes as T} from 'react';
import {browserHistory, router} from 'react-router';
import AuthService from 'utils/AuthService';
import {connect} from 'react-redux';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn, TableFooter} from 'material-ui/Table';
import FlatButton from 'material-ui/FlatButton';
import IconButton from 'material-ui/IconButton';
import RaisedButton from 'material-ui/RaisedButton';
import ProjectDialog from './ProjectDialog';

export class ProjectPage extends React.Component {
  static propTypes = {
    location: T.object,
    auth: T.instanceOf(AuthService)
  };
  constructor(props, context) {
    super(props, context);
    this.state = {
      projects: [],
      openProjectDialog: false,
      selectedProject: null
    };
  }
  componentDidMount() {
    this.getProjects();
  }
  getProjects() {
    const {api} = this.props;
    api.projects.getAll(data => {
      this.setState({projects: data.projects});
    }, err => {
      console.log(err);
      this.setState({projects: []});
    });
  }
  onOpenProjectDialog = project => {
    this.setState({
      selectedProject: project,
      openProjectDialog: true
    });
  };
  onCloseProjectDialog = () => {
    this.setState({
      openProjectDialog: false
    });
  };
  render() {
    const {projects, selectedProject, openProjectDialog} = this.state;

    return (
      <div>
        <Table multiSelectable={true} onRowSelection={this.handleSelection}>
          <TableHeader enableSelectAll={true} displaySelectAll={true}>
            <TableRow>
              <TableHeaderColumn>ID</TableHeaderColumn>
              <TableHeaderColumn>Name</TableHeaderColumn>
              <TableHeaderColumn>Client</TableHeaderColumn>
              <TableHeaderColumn>Active</TableHeaderColumn>
              <TableHeaderColumn>Created</TableHeaderColumn>
              <TableHeaderColumn>Updated By</TableHeaderColumn>
              <TableHeaderColumn></TableHeaderColumn>
              <TableHeaderColumn>Events</TableHeaderColumn>
              <TableHeaderColumn>Register</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody deselectOnClickaway={false}>
            {projects.map((project, i) =>
              <TableRow key={project.projectid} selectable={true}>
                <TableRowColumn>{project.projectid} </TableRowColumn>
                <TableRowColumn>{project.name}</TableRowColumn>
                <TableRowColumn>{project.client}</TableRowColumn>
                <TableRowColumn>{project.active}</TableRowColumn>
                <TableRowColumn>{project.created}</TableRowColumn>
                <TableRowColumn>{project.updatedby}</TableRowColumn>
                <TableHeaderColumn>
                  <FlatButton
                    label="Edit"
                    primary={true}
                    onTouchTap={() => this.onOpenProjectDialog(project)}
                  />
                </TableHeaderColumn>
                <TableRowColumn>
                  <FlatButton
                    label="Events"
                    primary={true}
                    onTouchTap={() => browserHistory.push(`/admin/event/${project.projectid}`)}
                  />
                </TableRowColumn>
                <TableRowColumn>
                  <RaisedButton
                    label="Register"
                    primary={true}
                    onTouchTap={() => browserHistory.push(`/landing/${project.projectid}`)}
                  />
                </TableRowColumn>
              </TableRow>
            )}
          </TableBody>
        </Table>
        {
          selectedProject &&
            <ProjectDialog
              api={this.props.api}
              open={openProjectDialog}
              project={selectedProject}
              handleClose={this.onCloseProjectDialog}
            />
        }
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    api: state.counter.api
  };
};

export default connect(mapStateToProps, undefined)(ProjectPage);
