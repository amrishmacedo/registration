import React, {PropTypes as T} from 'react';
import {browserHistory, router} from 'react-router';
import AuthService from 'utils/AuthService';
import {connect} from 'react-redux';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn, TableFooter} from 'material-ui/Table';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import RecordDialog from './RecordDialog';
import EventDialog from './EventDialog';
import {convertToLocalTime} from 'utils/timezoneCity';

export class EventPage extends React.Component {
  static propTypes = {
    location: T.object,
    auth: T.instanceOf(AuthService)
  };
  constructor(props, context) {
    super(props, context);
    this.state = {
      openUploadDialog: false,
      openEventDialog: false,
      events: [],
      selectedEvent: null,
      openRecordSnackbar: false,
      snackbarMessage: ''
    };
  }
  componentDidMount() {
    const {id, api} = this.props;
    const self = this;

    if (id) {
      api.events.getAll(id, data => {
        const events = data.events.map(event => {
          event.validRecord = self.isValidRecord(event.record);
          return event;
        });
        self.setState({events: events});
      }, err => {
        console.log(err);
      });
    }
  }
  isValidRecord(record) {
    if (record && record.url) {
      if (record.url.startsWith('https://firebasestorage.googleapis.com')) {
        return true;
      }
    }
    return false;
  }
  onOpenUploadDialog = event => {
    this.setState({
      selectedEvent: event,
      openUploadDialog: true
    });
  };
  onOpenEventDialog = event => {
    this.setState({
      selectedEvent: event,
      openEventDialog: true
    });
  };
  onCloseEventDialog = () => {
    this.setState({
      openEventDialog: false
    });
  };
  handleClose = () => {
    this.setState({
      openUploadDialog: false
    });
  };
  render() {
    const {events, openUploadDialog, openEventDialog, selectedEvent} = this.state;
    const data = events.reduce((prev, event) => {
      event.localTime = convertToLocalTime(event.startdatetime, event.timezone, 'YYYY/MM/DD HH:mm A');
      prev.push(event);
      return prev;
    }, []);

    return (
      <div>
        <Table>
          <TableHeader enableSelectAll={false} displaySelectAll={false}>
            <TableRow>
              <TableHeaderColumn>Name</TableHeaderColumn>
              <TableHeaderColumn>Start Date Time</TableHeaderColumn>
              <TableHeaderColumn>Duration</TableHeaderColumn>
              <TableHeaderColumn>Guests</TableHeaderColumn>
              <TableHeaderColumn>Edit</TableHeaderColumn>
              <TableHeaderColumn>Record</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody>
            {data.map((event, i) =>
              <TableRow key={event.event_id} selectable={true}>
                <TableRowColumn>{event.name}</TableRowColumn>
                <TableRowColumn>{event.localTime + ' ' + event.timezone}</TableRowColumn>
                <TableRowColumn>{event.duration}</TableRowColumn>
                <TableRowColumn>
                  {
                    event.guests ?
                      <RaisedButton
                        label={event.guests.length}
                        primary={true}
                        onTouchTap={() => this.onOpenEventDialog(event)}
                      /> :
                      '0'
                  }
                </TableRowColumn>
                <TableRowColumn>
                  <RaisedButton
                    label="Edit" primary={true}
                    onTouchTap={() => this.onOpenEventDialog(event)}
                  />
                </TableRowColumn>
                <TableRowColumn>
                  <FlatButton
                    label={event.validRecord ? 'Reload Record' : 'Load Record'}
                    primary={true}
                    onTouchTap={() => this.onOpenUploadDialog(event)}
                  />
                </TableRowColumn>
              </TableRow>
            )}
          </TableBody>
        </Table>
        {
          selectedEvent &&
            <EventDialog
              api={this.props.api}
              open={openEventDialog}
              event={selectedEvent}
              handleClose={this.onCloseEventDialog}
            />
        }
        {
          selectedEvent &&
            <RecordDialog
              api={this.props.api}
              open={openUploadDialog}
              event={selectedEvent}
              completeUpload={this.handleClose}
            />
        }
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    api: state.counter.api
  };
};

export default connect(mapStateToProps, undefined)(EventPage);
