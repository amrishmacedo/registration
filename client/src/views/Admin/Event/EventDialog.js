import React from 'react';
import moment from 'moment-timezone';
import Dialog from 'material-ui/Dialog';
import {Field, reduxForm} from 'redux-form';
import TextField from 'material-ui/TextField';
import DatePicker from 'material-ui/DatePicker';
import TimePicker from 'material-ui/TimePicker';
import FlatButton from 'material-ui/FlatButton';
import {convertToLocalTime, getTimeOffset} from 'utils/timezoneCity';

export class EventDialog extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      startDateTime: null,
      eventName: null,
      openAlertDialog: false
    };
  }
  handleDateChange = (e, date) => {
    const {event} = this.props;
    const {startDateTime} = this.state;
    const eventTime = startDateTime ?
        startDateTime :
        new Date(convertToLocalTime(event.startdatetime, event.timezone, 'YYYY/MM/DD HH:mm'));

    eventTime.setYear(date.getYear() + 1900);
    eventTime.setMonth(date.getMonth());
    eventTime.setDate(date.getDate());

    this.setState({
      startDateTime: eventTime
    });
  };
  handleTimeChange = (e, date) => {
    this.setState({
      startDateTime: date
    });
  };
  handleNameChange = (e, name) => {
    this.setState({
      eventName: name
    });
  };
  handleEventChange = () => {
    const {event} = this.props;
    const {startDateTime, eventName} = this.state;
    const data = {
      event_id: event.event_id,
      project_key: event.project_key
    };

    if (startDateTime || eventName) {
      if (startDateTime) {
        const fromDate = moment.utc(event.startdatetime);
        const toDate = moment.utc(convertToLocalTime(event.startdatetime, event.timezone, 'YYYY/MM/DD HH:mm'));
        const minuteDiff = toDate.diff(fromDate, 'minutes');
        const hourDuration = Math.floor(minuteDiff / 60);
        const minuteDuration = minuteDiff % 60;
        const duration = moment.duration({hour: hourDuration, minute: minuteDuration});
        const localTime = moment(startDateTime);
        localTime.subtract(duration);

        data.startdatetime = localTime.format('MM/DD/YYYY HH:mm') + 'Z';
        data.duration = event.duration;
      }
      if (eventName) {
        data.name = eventName;
      }

      this.props.api.events.update(data, () => {
        this.alertText = 'The start date & time was updated.';
        this.setState({openAlertDialog: true});
      }, err => {
        this.alertText = err.message;
        this.setState({openAlertDialog: true});
      });
    }
  };
  handleCloseAlert = () => {
    this.setState({openAlertDialog: false});
  };
  onCloseDialog = () => {
    this.state = {
      startDateTime: null,
      openAlertDialog: false
    };
    this.props.handleClose();
  };
  render() {
    const {event, open} = this.props;
    const {openAlertDialog, startDateTime} = this.state;
    const eventTime = startDateTime ?
        startDateTime :
        new Date(convertToLocalTime(event.startdatetime, event.timezone, 'YYYY/MM/DD HH:mm'));
    const eventName = this.state.projectName || event.name;

    const actions = [
      <FlatButton
        label="Close"
        primary={true}
        onTouchTap={this.onCloseDialog}
      />,
      <FlatButton
        label="Submit"
        primary={true}
        keyboardFocused={true}
        onTouchTap={this.handleEventChange}
      />
    ];
    const alertActions = [
      <FlatButton
        label="Close"
        primary={true}
        keyboardFocused={true}
        onTouchTap={this.handleCloseAlert}
      />
    ];

    return (
      <Dialog
        title="Edit Event"
        open={open}
        modal={true}
        actions={actions}
      >
        <div>
          <TextField
            id="id-event-name"
            floatingLabelText="Name"
            defaultValue={eventName}
            onChange={this.handleNameChange}
          />
          <DatePicker
            floatingLabelText="Start Date"
            value={eventTime}
            onChange={this.handleDateChange}
          />
          <TimePicker
            floatingLabelText={'Start Time ' + event.timezone}
            format="24hr"
            value={eventTime}
            onChange={this.handleTimeChange}
          />
          <TextField
            id="id-event-duration"
            floatingLabelText="Duration"
            defaultValue={event.duration}
            disabled={true}
          />
        </div>
        <Dialog open={openAlertDialog} actions={alertActions}>
          <div>{this.alertText}</div>
        </Dialog>
      </Dialog>
    );
  }
}

export default EventDialog;
