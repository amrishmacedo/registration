import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import firebase from 'firebase';
import LinearProgress from 'material-ui/LinearProgress';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import {Grid, Row, Col} from 'react-bootstrap';

const Dropzone = require('react-dropzone');

const style = {
  margin: 12
};

export class RecordDialog extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      droppedFile: null,
      uploadStatus: '',
      uploadProgress: 0,
      files: []
    };

    // Get a reference to the storage service, which is used to create references in your storage bucket
    this.storage = firebase.storage();
    this.storageTarget = 'firebase';
  }
  uploadToFirebase(file) {
    // Create a root reference
    const storageRef = firebase.storage().ref();
    const recordPath = `records/${file.name}`;
    const recordVideoRef = storageRef.child(recordPath);
    const uploadTask = recordVideoRef.put(file);
    const that = this;
    let uploadStatus = '';

    // Register three observers:
    // 1. 'state_changed' observer, called any time the state changes
    // 2. Error observer, called on failure
    // 3. Completion observer, called on successful completion
    uploadTask.on('state_changed', snapshot => {
      // Observe state change events such as progress, pause, and resume
      // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
      const progress = Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
      switch (snapshot.state) {
        case firebase.storage.TaskState.PAUSED:
          uploadStatus = 'Upload is paused';
          break;
        case firebase.storage.TaskState.RUNNING:
          uploadStatus = 'Upload is running';
          break;
        default:
          uploadStatus = 'Invalid state';
          break;
      }
      this.setState({
        uploadProgress: progress,
        uploadStatus
      });
    }, error => {
      this.setState({
        uploadProgress: 0,
        uploadStatus: error.code
      });
    }, () => {
      // Handle successful uploads on complete
      that.updateMetaData(recordVideoRef, file, that.props.event, (err, url) => {
        if (err) {
          that.setState({
            uploadStatus: `Upload failed. ${JSON.stringify(err)}`
          });
        } else {
          const record = {
            fileName: file.name,
            url
          };
          that.updateEventRecord(record, err => {
            if (err) {
              that.setState({
                uploadStatus: `Upload failed. ${JSON.stringify(err)}`
              });
            } else {
              that.setState({
                uploadStatus: 'Upload completed'
              });
            }
          });
        }
      });
    });
  }
  deleteFromFirebase(event) {
    if (event.record === undefined || event.record.fileName === undefined) {
      return this.setState({uploadStatus: 'No record to delete'});
    }

    const that = this;
    const storageRef = firebase.storage().ref();
    const recordPath = `records/${event.record.fileName}`;
    const recordVideoRef = storageRef.child(recordPath);

    recordVideoRef.delete().then(snapshot => {
      const record = {
        eventId: event.id,
        fileName: '',
        url: ''
      };
      that.updateEventRecord(record, err => {
        if (err) {
          that.setState({
            uploadStatus: `Delete failed. ${JSON.stringify(err)}`
          });
        } else {
          that.setState({
            uploadStatus: 'Delete completed'
          });
        }
      });
    }).catch(err => {
      that.setState({
        uploadStatus: `Delete failed. ${JSON.stringify(err)}`
      });
    });
  }
  updateMetaData(recordVideoRef, file, event, callback) {
    // Create file metadata to update
    const recordMetadata = {
      contentType: file.type,
      customMetadata: {
        fileName: file.name,
        eventId: event.id
      }
    };

    // Update metadata properties
    recordVideoRef.updateMetadata(recordMetadata).then(metadata => {
      if (metadata.downloadURLs.length) {
        callback(null, metadata.downloadURLs[0]);
      } else {
        callback(null, '');
      }
    }).catch(err => {
      // Uh-oh, an error occurred!
      callback(err);
    });
  }
  updateEventRecord(record, callback) {
    const {api, event} = this.props;

    record.eventId = event.event_id;

    api.events.updateRecord(event, record).then(record => {
      callback(null);
    }).catch(err => {
      callback(err);
    });
  }
  uploadFile(file) {
    const {api, event} = this.props;
    api.records.upload(event, file, records => {
      console.log(records);
    }, err => {
      console.log(err);
    });
  }
  isValidRecord(record) {
    if (record && record.url) {
      if (record.url.startsWith('https://firebasestorage.googleapis.com')) {
        return true;
      }
    }
    return false;
  }
  onDrop(files) {
    this.setState({
      files,
      droppedFile: files[0]
    });
  }
  onOpenClick() {
    this.setState({
      droppedFile: null
    });
    this.refs.dropzone.open();
  }
  onRemoveClick() {
    const {files} = this.state;
    if (files.length === 0) {
      return;
    }
    this.setState({
      files: [],
      droppedFile: null
    });
  }
  onSaveClick() {
    const {droppedFile} = this.state;
    if (droppedFile) {
      if (this.storageTarget === 'firebase') {
        this.uploadToFirebase(droppedFile);
      } else {
        this.uploadFile(droppedFile);
      }
    }
  }
  onDeleteClick() {
    this.deleteFromFirebase(this.props.event);
  }
  onCloseDialog = () => {
    this.setState({
      droppedFile: null,
      uploadStatus: '',
      uploadProgress: 0,
      files: []
    });
    this.props.completeUpload();
  };
  render() {
    const {event, open, handleClose} = this.props;
    const {droppedFile, uploadProgress, uploadStatus} = this.state;
    const actions = [
      <FlatButton
        label="Close"
        primary={true}
        keyboardFocused={true}
        onTouchTap={this.onCloseDialog}
      />
    ];
    let videoSrc = '';
    let fileName = '';
    if (droppedFile) {
      videoSrc = droppedFile.preview;
      fileName = droppedFile.name;
    } else if (event) {
      if (this.isValidRecord(event.record)) {
        fileName = event.record.fileName;
        videoSrc = event.record.url;
      } else {
        fileName = '';
        videoSrc = '';
      }
    } else {
      fileName = '';
      videoSrc = '';
    }

    return (
      <Dialog
        title="Upload record file"
        actions={actions}
        modal={true}
        open={open}
      >
        <Grid fluid>
          <Row style={style}>
            File Name: {fileName}
          </Row>
          <Row style={style}>
            <Col xs={12} md={12}>
              <Dropzone ref="dropzone" onDrop={this.onDrop.bind(this)} style={{width: '640px', height: '320px'}} >
              {
                videoSrc === '' ?
                  'Try dropping some files here, or click to select files to upload.' :
                    <video width="640px" height="320px" controls>
                      <source id="videoSource" src={videoSrc} type="video/mp4"/>
                    </video>
              }
              </Dropzone>
            </Col>
          </Row>
          <Row>
            <Col xs={12} md={12}>
              <div style={{textAlign: 'center'}}>
                <RaisedButton label="Open" primary={true} style={style} onTouchTap={this.onOpenClick.bind(this)} />
                <RaisedButton label="Remove" primary={true} style={style} onTouchTap={this.onRemoveClick.bind(this)} />
                <RaisedButton label="Upload" primary={true} style={style} onTouchTap={this.onSaveClick.bind(this)} />
                <RaisedButton label="Delete" secondary={true} style={style} onTouchTap={this.onDeleteClick.bind(this)} />
              </div>
            </Col>
          </Row>
          <Row style={style}>
            <LinearProgress mode="determinate" value={uploadProgress} />
          </Row>
          <Row style={style}>
            <div>{uploadStatus} {uploadProgress ? uploadProgress + '% done.' : ''}</div>
          </Row>
        </Grid>
      </Dialog>
    );
  }
}

export default RecordDialog;
