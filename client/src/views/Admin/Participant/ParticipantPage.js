import React, {PropTypes as T} from 'react';
import {browserHistory, router} from 'react-router';
import ApiService from 'utils/ApiService';
import {connect} from 'react-redux';
import ParticipantList from './ParticipantList';
import ParticipantDetail from './ParticipantDetail';

export class ParticipantPage extends React.Component {
  static propTypes = {
    location: T.object,
    api: T.instanceOf(ApiService)
  };
  render() {
    const {id, api} = this.props;

    return (
      <div>
        {
          (typeof id === 'undefined') ?
            <ParticipantList api={api} location={location}/> : <ParticipantDetail api={api} id={id}/>
        }
      </div>
    );
  }
}

export default ParticipantPage;
