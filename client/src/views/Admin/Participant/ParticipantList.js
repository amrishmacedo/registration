import React, {PropTypes as T} from 'react';
import {browserHistory} from 'react-router';
import ApiService from 'utils/ApiService';
import {connect} from 'react-redux';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn, TableFooter} from 'material-ui/Table';
import FlatButton from 'material-ui/FlatButton';

export class ParticipantList extends React.Component {
  static propTypes = {
    location: T.object,
    api: T.instanceOf(ApiService)
  };
  constructor(props, context) {
    super(props, context);
    this.state = {
      participants: []
    };
  }
  componentWillMount() {
    this.getParticipants();
  }
  getParticipants() {
    const {api} = this.props;
    api.participants.getAll(participants => {
      this.setState({participants: participants});
    }, err => {
      this.setState({participants: []});
    });
  }
  render() {
    const {location} = this.props;
    const {participants} = this.state;

    participants.map(participant => {
      participant.eventList = [];
      if (participant.event) {
        const ev = {};
        for (const property in participant.event) {
          if (participant.event.hasOwnProperty(property)) {
            ev.id = property;
            ev.flag = participant.event[property];
          }
          participant.eventList.push(ev);
        }
      }
      return participant;
    });

    return (
      <div>
        <Table multiSelectable={false} onRowSelection={this.handleSelection}>
          <TableHeader enableSelectAll={false} displaySelectAll={false}>
            <TableRow>
              <TableHeaderColumn>ID</TableHeaderColumn>
              <TableHeaderColumn>Email</TableHeaderColumn>
              <TableHeaderColumn>Name</TableHeaderColumn>
              <TableHeaderColumn>Created</TableHeaderColumn>
              <TableHeaderColumn>Updated By</TableHeaderColumn>
              <TableHeaderColumn>Auth0 ID</TableHeaderColumn>
              <TableHeaderColumn>Events</TableHeaderColumn>
              <TableHeaderColumn></TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody deselectOnClickaway={false}>
            {participants.map((participant, i) =>
              <TableRow key={i} selectable={true}>
                <TableRowColumn>{participant.id} </TableRowColumn>
                <TableRowColumn>{participant.email} </TableRowColumn>
                <TableRowColumn>{participant.name}</TableRowColumn>
                <TableRowColumn>{participant.created}</TableRowColumn>
                <TableRowColumn>{participant.updatedby}</TableRowColumn>
                <TableRowColumn>{participant.auth0_id}</TableRowColumn>
                <TableRowColumn>{participant.eventList.length}</TableRowColumn>
                <TableHeaderColumn>
                  <FlatButton
                    label="Detail"
                    primary={true}
                    onTouchTap={() => browserHistory.push(location + '/' + participant.id)}
                  />
                </TableHeaderColumn>
              </TableRow>
            )}
          </TableBody>
        </Table>
      </div>
    );
  }
}

export default ParticipantList;
