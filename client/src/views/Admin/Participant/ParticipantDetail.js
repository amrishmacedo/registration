import React, {PropTypes as T} from 'react';
import {browserHistory, router} from 'react-router';
import ApiService from 'utils/ApiService';
import {connect} from 'react-redux';
import FlatButton from 'material-ui/FlatButton';
import {Grid, Row, Col} from 'react-bootstrap';

export class UserDetail extends React.Component {
  static propTypes = {
    location: T.object,
    api: T.instanceOf(ApiService)
  };
  constructor(props, context) {
    super(props, context);
    this.state = {
      participant: {}
    };
  }
  componentWillMount() {
    this.getUser();
  }
  getUser() {
    const {api, id} = this.props;
    api.participants.get(id, participant => {
      this.setState({participant: participant});
    }, err => {
      this.setState({participant: {}});
    });
  }
  render() {
    const {participant} = this.state;

    return (
      <div style={{textAlign: 'center'}}>
        <Grid>
          <Row>
            <Col md={3} />
            <Col md={3}><b>Email :</b></Col>
            <Col md={3}>{participant.email}</Col>
            <Col md={3} />
          </Row>
        </Grid>
      </div>
    );
  }
}

export default UserDetail;


