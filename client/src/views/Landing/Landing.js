import React, {PropTypes as T} from 'react';
import {browserHistory} from 'react-router';
import {connect} from 'react-redux';
import RaisedButton from 'material-ui/RaisedButton';
import AuthService from 'utils/AuthService';
import {Row, Col, Image} from 'react-bootstrap';
import {List, ListItem} from 'material-ui/List';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import ContentInbox from 'material-ui/svg-icons/content/inbox';
import ActionGrade from 'material-ui/svg-icons/action/grade';
import ContentSend from 'material-ui/svg-icons/content/send';
import styles from './styles.module.css';

export class Landing extends React.Component {
  static propTypes = {
    auth: T.instanceOf(AuthService)
  };
  constructor(props, context) {
    super(props, context);
    this.state = {
      profile: {}
    };
  }
  componentWillMount() {
    const {auth} = this.props;
    this.setState(
      {profile: auth.getProfile()}
    );
  }
  onLogoutClicked = () => {
    this.props.auth.logout();
    browserHistory.push('/login');
  };
  onAdminClicked = () => {
    browserHistory.push('/admin');
  };
  render() {
    const {auth} = this.props;
    const {profile} = this.state;
    const {address} = profile.user_metadata || {};

    return (
      <div className={styles.pageContent} style={{padding: 20}}>
        <Row>
          <Col md={6} mdOffset={3}>
            <Card expanded={true}>
              <CardHeader
                title={profile.name}
                avatar={profile.picture}
              />
              <CardMedia expandable={true} >
                <List>
                  <ListItem primaryText={profile.email} leftIcon={<ContentInbox />} />
                  <ListItem primaryText={profile.nickname} leftIcon={<ActionGrade />} />
                  <ListItem primaryText={address} leftIcon={<ContentSend />} />
                </List>
              </CardMedia>
              <CardActions>
                <RaisedButton label="Logout" primary={true} onTouchTap={this.onLogoutClicked} />
                {
                  auth.isAdmin() &&
                    <RaisedButton label="Admin" primary={true} onTouchTap={this.onAdminClicked} />
                }
              </CardActions>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    api: state.counter.api
  };
};

export default connect(mapStateToProps, null)(Landing);
