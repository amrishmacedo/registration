import React, {PropTypes as T} from 'react';
import {browserHistory} from 'react-router';
import AuthService from 'utils/AuthService';
import RaisedButton from 'material-ui/RaisedButton';

export class Login extends React.Component {
  static propTypes = {
    auth: T.instanceOf(AuthService)
  };
  render() {
    const {auth} = this.props;
    return (
      <div style={{textAlign: 'center'}}>
        <h2>Login</h2>
        <RaisedButton label="Login" primary={true} onTouchTap={auth.login.bind(this)}/>
      </div>
    );
  }
}

export default Login;
