import React, {PropTypes as T} from 'react';
import {connect} from 'react-redux';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import AuthService from 'utils/AuthService';
import ApiService from 'utils/ApiService';
import {setApi} from 'core/Actions';

const muiTheme = getMuiTheme({
  palette: {
    accent1Color: 'rgba(1, 92, 2, 29)'
  }
});

export class Container extends React.Component {
  static contextTypes = {
    router: T.object
  };

  static propTypes = {
    auth: T.instanceOf(AuthService)
  };

  constructor(props, context) {
    super(props, context);
    this.props.setApi(new ApiService(this.props.route.auth));
  }

  render() {
    let children = null;
    if (this.props.children) {
      children = React.cloneElement(this.props.children, {
        auth: this.props.route.auth
      });
    }

    return (
      <MuiThemeProvider muiTheme={muiTheme}>
        <div className="App">
          {children}
        </div>
      </MuiThemeProvider>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setApi: api => dispatch(setApi(api))
  };
};

export default connect(undefined, mapDispatchToProps)(Container);
