import {SET_API} from './Actions';

const counterInitialState = {
  api: null
};

export const counter = (state = counterInitialState, action) => {
  switch (action.type) {
    case SET_API:
      return Object.assign({}, state, {
        api: action.api
      });
    default:
      return state;
  }
};

export const extra = (state = {value: 'this_is_extra_reducer'}, action) => {
  switch (action.type) {
    default:
      return state;
  }
};