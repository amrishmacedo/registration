import React from 'react';
import ReactDOM from 'react-dom';
import App from 'containers/App/App';
import {browserHistory} from 'react-router';
import makeRoutes from './routes';
import injectTapEventPlugin from 'react-tap-event-plugin';
import {syncHistoryWithStore, routerReducer, routerMiddleware, push} from 'react-router-redux';
import {Provider} from 'react-redux';
import {combineReducers, createStore, applyMiddleware} from 'redux';
import * as reducers from 'core/Reducers';
import favicon from 'images/favicon.ico';
import 'bootstrap/dist/css/bootstrap.css';
import './app.css';

// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin();

const routes = makeRoutes();

const mountNode = document.querySelector('#root');

const reducer = combineReducers({
  ...reducers,
  routing: routerReducer
});

const middleware = routerMiddleware(browserHistory);

// Add the reducer to your store on the `routing` key
const store = createStore(
  reducer,
  applyMiddleware(middleware)
);

// Create an enhanced history that syncs navigation events with the store
const history = syncHistoryWithStore(browserHistory, store);
history.listen(location => console.log(location.pathname));

ReactDOM.render(
  <Provider store={store}>
    <App history={history}
         routes={routes}/>
  </Provider>,
  mountNode);
