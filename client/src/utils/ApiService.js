import fetch from 'isomorphic-fetch';
import firebase from 'firebase';

export default class ApiService {
  constructor(auth) {
    this.auth = auth;
    auth.api = this;
  }

  _makeSecuredCall(route, options, callback, error) {
    this.auth.fetch(route, options)
      .then(response => {
        callback(response);})
      .catch(e => {
        error(e);
      });
  }

  _makeCall(route, options, callback, error) {
    fetch(route, options)
      .then(response => {
        if (response.status === 200) {
          return response.json();
        }
        throw new Error(response.json());
      })
      .then(response => { callback(response); })
      .catch(e => {
        error(e);
      });
  }

  test = {
    public: (callback, error) => {
      const route = `/api/public`;
      const options = {
        credentials: 'same-origin'
      };
      this._makeCall(route, options, callback, error);
    },
    private: (callback, error) => {
      const route = `/api/private`;
      const options = {
        credentials: 'same-origin'
      };
      this._makeSecuredCall(route, options, callback, error);
    }
  };

  profiles = {
    /**
     *
     * @param profile
     * @param callback
     * @param error
     */
    update: (profile, callback, error) => {
      const route = `/api/profile`;
      const options = {
        method: 'POST',
        credentials: 'same-origin',
        body: JSON.stringify(profile)
      };
      this._makeSecuredCall(route, options, callback, error);
    },
    /**
     *
     * @param profile
     * @param callback
     * @param error
     */
    remove: (profile, callback, error) => {
      const route = `/api/profile`;
      const options = {
        method: 'DELETE',
        credentials: 'same-origin'
      };
      this._makeSecuredCall(route, options, callback, error);
    }
  };

  projects = {
    getImageUrl(imageName, callback) {
      const storageRef = firebase.storage().ref();
      const imagePath = `images/${imageName}`;
      const imageRef = storageRef.child(imagePath);

      imageRef.getDownloadURL().then(url => {
        // Insert url into an <img> tag to "download"
        callback(null, url);
      }).catch(error => {
        // A full list of error codes is available at
        // https://firebase.google.com/docs/storage/web/handle-errors
        switch (error.code) {
          case 'storage/object_not_found':
            // File doesn't exist
            break;

          case 'storage/unauthorized':
            // User doesn't have permission to access the object
            break;

          case 'storage/canceled':
            // User canceled the upload
            break;

          case 'storage/unknown':
            // Unknown error occurred, inspect the server response
            break;

          default:
            // Unknown state
        }
        callback(error, '');
      });
    },
    /**
     *
     * @param callback
     * @param error
     */
    getAll: (callback, error) => {
      const route = `/api/projects`;
      const options = {
        credentials: 'same-origin'
      };
      this._makeCall(route, options, callback, error);
    },
    /**
     *
     * @param projectId
     * @param callback
     * @param error
     */
    get: (projectId, callback, error) => {
      const route = `/api/project/${projectId}`;
      const options = {
        credentials: 'same-origin'
      };
      this._makeCall(route, options, callback, error);
    },
    /**
     * Get project with given user's event setting
     *
     * @param projectId
     * @param profile
     * @param callback
     * @param error
     */
    getForUser: (projectId, profile, callback, error) => {
      const route = `/api/project/${projectId}/events?email=${profile.email}`;
      const options = {
        credentials: 'same-origin',
      };
      this._makeSecuredCall(route, options, callback, error);
    },
    /**
     * Update project's properties such as name
     *
     * @param projectKey
     * @param data
     * @param callback
     * @param error
     */
    update: (projectKey, data, callback, error) => {
      const route = `/api/private/project/${projectKey}`;
      const options = {
        method: 'PUT',
        credentials: 'same-origin',
        body: JSON.stringify(data)
      };
      this._makeSecuredCall(route, options, callback, error);
    }
  };

  events = {
    /**
     *
     * @param projectId
     * @param callback
     * @param error
     */
    getAll: (projectId, callback, error) => {
      const route = `/api/events/${projectId}`;
      const options = {
        credentials: 'same-origin'
      };
      this._makeSecuredCall(route, options, callback, error);
    },
    /**
     *
     * @param event
     * @param callback
     * @param error
     */
    update: (event, callback, error) => {
      const route = `/api/events/${event.event_id}`;
      const options = {
        method: 'PUT',
        credentials: 'same-origin',
        body: JSON.stringify(event)
      };
      this._makeSecuredCall(route, options, callback, error);
    },
    /**
     *
     * @param event
     * @param record
     * @param callback
     * @param error
     * @returns {*}
     */
    updateRecord: (event, record, callback, error) => {
      const url = `/api/events/${event.event_id}/record`;
      const body = {
        project_key: event.project_key,
        record: record
      };
      const options = {
        method: 'PUT',
        credentials: 'same-origin',
        body: JSON.stringify(body)
      };
      return this._makeSecuredCall(url, options, callback, error);
    },
    /**
     *
     * @param projectId
     * @param data
     * @param callback
     * @param error
     */
    register: (projectId, data, callback, error) => {
      const route = `/api/project/${projectId}/events/register`;
      const options = {
        method: 'POST',
        credentials: 'same-origin',
        body: JSON.stringify(data)
      };
      this._makeSecuredCall(route, options, callback, error);
    }
  };

  records = {
    /**
     *
     * @param event
     * @param file
     * @param callback
     * @param error
     */
    upload: (event, file, callback, error) => {
      const formData = new FormData();
      formData.append('file', file);
      formData.append('file-size', file.size);
      const route = '/api/records';
      const options = {
        method: 'POST',
        credentials: 'same-origin',
        body: formData
      };
      this._makeCall(route, options, callback, error);
    }
  };

  users = {
    /**
     * Get all users from auth0 database
     * @param callback
     * @param error
     */
    getAll: (callback, error) => {
      const route = `/api/users`;
      const options = {
        credentials: 'same-origin'
      };
      this._makeSecuredCall(route, options, callback, error);
    },
    get: (userId, callback, error) => {
      const route = `/api/users/${userId}`;
      const options = {
        credentials: 'same-origin'
      };
      this._makeSecuredCall(route, options, callback, error);
    }
  };

  participants = {
    /**
     * Get all participants from auth0 database
     * @param callback
     * @param error
     */
    getAll: (callback, error) => {
      const route = `/api/participants`;
      const options = {
        credentials: 'same-origin'
      };
      this._makeSecuredCall(route, options, callback, error);
    },
    get: (participantId, callback, error) => {
      const route = `/api/participants/${participantId}`;
      const options = {
        credentials: 'same-origin'
      };
      this._makeSecuredCall(route, options, callback, error);
    }
  };

  calendars = {
    getList: (callback, error) => {
      const route = `/api/calendar/list`;
      const options = {
        method: 'GET',
        credentials: 'same-origin'
      };
      this._makeSecuredCall(route, options, callback, error);
    }
  }
}
