import {EventEmitter} from 'events';
import {isTokenExpired} from './jwtHelper';
import Auth0Lock from 'auth0-lock';
import {Auth0} from 'auth0-js';
import {browserHistory} from 'react-router';
import LogoImg from 'images/logo.png';
import firebase from 'firebase';

export default class AuthService extends EventEmitter {
  constructor(clientId, domain) {
    super();
    // Configure Auth0
    const options = {
      theme: {
        logo: LogoImg,
        primaryColor: '#b81b1c'
      },
      languageDictionary: {
        title: 'RVIBE'
      },
      auth: {
        redirectUrl: `${window.location.origin}/login`,
        responseType: 'token'
      },
      additionalSignUpFields: [
        {
          name: 'full_name',
          placeholder: 'Enter your full name'
        }
      ],
      usernameStyle: 'email'
    };
    this.lock = new Auth0Lock(clientId, domain, options);
    // Add callback for lock `authenticated` event
    this.lock.on('authenticated', this._doAuthentication.bind(this));
    // Add callback for lock `authorization_error` event
    this.lock.on('authorization_error', this._authorizationError.bind(this));
    // Binds login functions to keep this context
    this.login = this.login.bind(this);
  }

  _doAuthentication(authResult) {
    // Saves the user token
    this.setToken(authResult.idToken);

    // Async loads the user profile data
    this.lock.getProfile(authResult.idToken, (error, profile) => {
      if (error) {
        browserHistory.push('/login');
      } else {
        this.api.profiles.update(profile, participant => {
          profile.participant = participant;
          this.setProfile(profile);

          let lastUrl = localStorage.getItem('redirect_after_login');
          if (lastUrl) {
            lastUrl = lastUrl.replace(/^"(.*)"$/, '$1');
            browserHistory.replace(lastUrl);
          } else {
            browserHistory.replace('/landing');
          }
        }, err => {
          console.log(err);
          browserHistory.push('/login');
        });
      }
    });
  }

  _authorizationError(error) {
    // Unexpected authentication error
    console.log('Authentication Error', error);
  }

  login() {
    this.lock.show();
  }

  loggedIn() {
    // Checks if there is a saved token and it's still valid
    const token = this.getToken();
    return Boolean(token) && !isTokenExpired(token);
  }

  setProfile(profile) {
    // Saves profile data to localStorage
    localStorage.setItem('profile', JSON.stringify(profile));
    // Triggers profile_updated event to update the UI
    this.emit('profile_updated', profile);
  }

  getProfile() {
    // Retrieves the profile data from localStorage
    const profile = localStorage.getItem('profile');
    return profile ? JSON.parse(localStorage.profile) : {};
  }

  isAdmin() {
    const profile = this.getProfile();
    const {roles} = profile.app_metadata || {};
    return Boolean(roles) && roles.indexOf('admin') > -1;
  }

  setToken(idToken) {
    // Saves user token to localStorage
    localStorage.setItem('id_token', idToken);
  }

  getToken() {
    // Retrieves the user token from localStorage
    return localStorage.getItem('id_token');
  }

  logout() {
    const self = this;

    // Clear user token and profile data from localStorage
    localStorage.removeItem('id_token');
    localStorage.removeItem('profile');
    localStorage.removeItem('redirect_after_login');

    if (firebase) {
      firebase.auth().signOut().then(() => {
        self.clearFbUser();
        self.emit('profile_removed');
      }, error => {
        console.log(error);
      });
    } else {
      // Triggers profile_updated event to update the UI
      self.emit('profile_removed');
    }
  }

  _checkStatus(response) {
    // Raises an error in case response status is not a success
    if (response.status >= 200 && response.status < 300) {
      return response;
    }

    const error = new Error(response.statusText);
    error.response = response;
    throw error;
  }

  fetch(url, options) {
    // Performs api calls sending the required authentication headers
    const headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    };

    if (this.loggedIn()) {
      headers.Authorization = 'Bearer ' + this.getToken();
    }

    return fetch(url, {
      headers,
      ...options
    })
      .then(this._checkStatus)
      .then(response => response.json());
  }

  setFbUser(fbUser) {
    // Saves user token to localStorage
    localStorage.setItem('fb_user', fbUser);
  }

  getFbUser() {
    // Retrieves the user token from localStorage
    return localStorage.getItem('fb_user');
  }

  clearFbUser() {
    // Saves user token to localStorage
    localStorage.removeItem('fb_user');
  }
}
