import moment from 'moment-timezone';

const timezoneCity = {
  UTC: 'Europe/London',
  EST: 'America/New_York'
};

/**
 *
 * @param aTime
 * @param timezone
 * @param format
 * @param offset
 */
const convertToLocalTime = (aTime, timezone, format, offset) => {
  const isoTime = new Date(aTime).toISOString();
  const tz = (timezone === undefined || !timezoneCity[timezone]) ? timezoneCity.UTC : timezoneCity[timezone];

  const localTime = moment(isoTime).tz(tz);
  if (format) {
    const formattedLocalTime = String(localTime.format(format));
    if (offset) {
      return formattedLocalTime + moment.tz(tz).format('z');
    }
    return formattedLocalTime;
  }

  return localTime;
};

const getTimeOffset = timezone => {
  const tz = (timezone === undefined || !timezoneCity[timezone]) ? timezoneCity.UTC : timezoneCity[timezone];
  return moment.tz(tz).format('z');
};

export {convertToLocalTime, getTimeOffset};
