import React, {PropTypes as T} from 'react';
import MenuItem from 'material-ui/MenuItem';
import Avatar from 'material-ui/Avatar';
import Popover from 'material-ui/Popover/Popover';
import IconButton from 'material-ui/IconButton';

export class ProfileMenu extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      open: false
    };
  }

  handleTouchTap = event => {
    event.preventDefault();

    this.setState({
      open: true,
      anchorEl: event.currentTarget
    });
  };

  handleRequestClose = () => {
    this.setState({
      open: false
    });
  };

  render() {
    const {name, picture, logout} = this.props;

    return (
      <div>
        <IconButton
          style={{'padding': 0}}
          onTouchTap={this.handleTouchTap}
         >
          <Avatar src={picture} />
        </IconButton>
         <Popover
          open={this.state.open}
          anchorEl={this.state.anchorEl}
          anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
          targetOrigin={{horizontal: 'left', vertical: 'top'}}
          onRequestClose={this.handleRequestClose}
         >
          <MenuItem primaryText={name}/>
          <MenuItem onTouchTap={logout} primaryText="Sign out"/>
         </Popover>
      </div>
    );
  }
}

export default ProfileMenu;
