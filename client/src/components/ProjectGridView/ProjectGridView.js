import React from 'react';
import $ from 'jquery';
import {Grid, Row, Col} from 'react-bootstrap';
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';
import {GridList, GridTile} from 'material-ui/GridList';
import StarBorder from 'material-ui/svg-icons/toggle/star-border';

const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around'
  },
  gridList: {
    display: 'flex',
    flexWrap: 'nowrap',
    overflowX: 'auto',
    cellHeight: '240px'
  },
  titleStyle: {
    color: 'rgb(0, 188, 212)'
  }
};

export class ProjectGridView extends React.Component {
  componentDidUpdate() {
    const {api, projects} = this.props;
    projects.forEach(project => {
      api.projects.getImageUrl(project.image, (err, url) => {
        const image = $('#image-' + project.projectid);
        if (err) {
          image.attr('src', '');
        } else {
          image.attr('src', url);
        }
      });
    });
  }
  _tileClick = (id) => {
    this.props.onTileClick(id);
  };

  render() {
    const {projects} = this.props;
    return (
      <div style={styles.root}>
        <Grid>
          <Row className="show-grid">
            <Toolbar>
              <ToolbarGroup>
                <ToolbarTitle text="Projects"/>
                <FontIcon className="muidocs-icon-custom-sort"/>
              </ToolbarGroup>
            </Toolbar>
          </Row>
          <Row className="show-grid">
            <br/>
            <p>Please select the project to register event</p>
          </Row>
          <Row>
            <GridList style={styles.gridList} cols={2.2}>
              {projects.map((project, i) => (
                <GridTile
                  key={project.projectid}
                  title={project.name}
                  actionIcon={<IconButton><StarBorder color="rgb(0, 188, 212)" /></IconButton>}
                  titleStyle={styles.titleStyle}
                  titleBackground="linear-gradient(to top, rgba(0,0,0,0.7) 0%,rgba(0,0,0,0.3) 70%,rgba(0,0,0,0) 100%)"
                  onTouchTap={this._tileClick.bind(this, project.projectid)}
                >
                  <img id={`image-${project.projectid}`}/>
                </GridTile>
              ))}
            </GridList>
          </Row>
        </Grid>
      </div>
    )
  }
}

export default ProjectGridView;