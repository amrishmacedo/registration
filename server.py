from __future__ import print_function

import os
from app import app


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=int(os.environ.get('PORT', 3001)))
